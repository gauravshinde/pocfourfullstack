import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/AuthSection/Login/Login';
import SignUp from './components/AuthSection/SignUp/SignUp';
import Otp from './components/AuthSection/Otp/Otp';
import Forgot from './components/AuthSection/Forgot/Forgot';
import mainDetailPage from './components/ParentComment/mainDetailPage';
import MainDashboard from './components/Dassboard/MainDashboard';
import PrivateRoute from './store/utils/PrivateRoute';
import DetailsPrivateRoute from './store/utils/DetailsPrivateRoute';

import MYSettins from './components/Settings/settingsMain';

// Provider and store
import store from './store/store';
import { Provider } from 'react-redux';
import setAuthToken from './store/utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import { logOutUser, set_current_user } from './store/actions/authActions';
// import MySettings from './components/Dassboard/InsidePage/MySettings';
import VendorDashboard from './pocsrcone/Components/VendorDashboard/VendorDashboard';
import VendorDiner from './pocsrcone/Components/VendorDiner/VendorDiner';

/***************************************
 *  JWT AUTHENTICATION
 ***************************************/
if (localStorage.jwtToken) {
  // SET AUTH TOKEN
  setAuthToken(localStorage.jwtToken);
  // SET CURRENT USER
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(set_current_user(decoded));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logOutUser());
    window.location.href = '/';
  }
}
/***************************************
 *  JWT AUTHENTICATION ENDS
 ***************************************/

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Route exact path='/' component={Login} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/signup' component={SignUp} />
        <Route exact path='/otp' component={Otp} />
        <Route exact path='/forgotpassword' component={Forgot} />

        <Switch>
          <PrivateRoute exact path='/dashboard' component={MainDashboard} />
          <PrivateRoute exact path='/mysettings' component={MYSettins} />
          <DetailsPrivateRoute
            exact
            path='/vendordashboard'
            component={VendorDashboard}
          />
          <DetailsPrivateRoute
            exact
            path='/add-diner'
            component={VendorDiner}
          />

          <DetailsPrivateRoute
            exact
            path='/maindetailpage'
            component={mainDetailPage}
          />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
