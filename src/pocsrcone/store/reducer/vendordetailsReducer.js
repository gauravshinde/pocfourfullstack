import { VENDOR_DETAILS } from "../types";

const initialState = {
  vendordetails: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case VENDOR_DETAILS:
      return {
        vendordetails: action.payload
      };
    default:
      return state;
  }
}
