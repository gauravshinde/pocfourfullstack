import { STATUS_CHANGE } from "../types";

const initialState = {
  statuschange: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case STATUS_CHANGE:
      return {
        statuschange: action.payload
      };
    default:
      return state;
  }
}
