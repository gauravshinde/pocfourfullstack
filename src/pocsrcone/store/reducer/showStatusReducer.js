import { GET_NOTSEATED_STATUS } from "../types";

const initialState = {
  notseated: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_NOTSEATED_STATUS:
      return {
        notseated: action.payload
      };
    default:
      return state;
  }
}
