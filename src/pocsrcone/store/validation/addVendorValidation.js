import isEmpty from "./is-empty";
import validator from "validator";

export const addVendorValidation = data => {
  let errors = {};

  data.vendor_name = !isEmpty(data.vendor_name) ? data.vendor_name : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  data.phone_number = !isEmpty(data.phone_number) ? data.phone_number : "";
  data.address = !isEmpty(data.address) ? data.address : "";
  data.username = !isEmpty(data.username) ? data.username : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.password2 = !isEmpty(data.password2) ? data.password2 : "";

  // vendorname

  if (!validator.isLength(data.vendor_name, { min: 2, max: 30 })) {
    errors.vendor_name = "Vendorname atleast 2 characters *";
  }

  if (validator.isEmpty(data.vendor_name)) {
    errors.vendor_name = "Vendorname is required *";
  }

  // Email Address

  if (!validator.isEmail(data.email)) {
    errors.email = "Email Is Invalid *";
  }

  if (validator.isEmpty(data.email)) {
    errors.email = "Email is required *";
  }

  // Phone Number

  if (!validator.isLength(data.phone_number, { min: 10, max: 10 })) {
    errors.phone_number = "Enter valid phone number*";
  }

  if (validator.isEmpty(data.phone_number)) {
    errors.phone_number = "phone number is required *";
  }

  // Address

  if (!validator.isLength(data.address, { min: 4, max: 150 })) {
    errors.address = "Enter address minimum 4 characters*";
  }

  if (validator.isEmpty(data.address)) {
    errors.address = "Address is required *";
  }

  // Username name

  if (!validator.isLength(data.username, { min: 2, max: 30 })) {
    errors.username = "Username atleast 2 characters *";
  }

  if (validator.isEmpty(data.username)) {
    errors.username = "Username is required *";
  }

  //passord

  if (!validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "password must be between 6 and 30 characters";
  }

  if (validator.isEmpty(data.password)) {
    errors.password = "Password field is required";
  }

  //Confirm passord

  // const { password, password2 } = this.state;
  // // perform all neccassary validations
  // if (password !== password2) {
  //   alert("Passwords don't match");
  // } else {

  //   if (!validator.equals(data.password2 !== data.password)) {
  //     errors.password2 = "Password does not match";
  //   }

  if (!validator.isLength(data.password2, { min: 6, max: 30 })) {
    errors.password2 = "Confirm password must be between 6 and 30 characters";
  }

  if (validator.isEmpty(data.password2)) {
    errors.password2 = "Confirm password field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
