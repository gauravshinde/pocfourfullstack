import axios from "axios";

import { VENDOR_LIST } from "../types";

//get vendor data

export const getVendor = () => dispatch => {
  axios
    .get("/vendor/all_vendor_list")
    .then(res =>
      dispatch({
        type: VENDOR_LIST,
        payload: res.data
      })
    )
    .catch(err => console.log(err));

  // .then(res => console.log(res));

  // .then(res =>
  //     dispatch({
  //       type: VENDOR_LIST,
  //       payload: res.data
  //     })
};
