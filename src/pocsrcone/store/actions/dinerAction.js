import axios from "axios";

import { DINNER_LIST, GET_ERRORS } from "../types";

//get vendor data

export const getDinner = venderId => async dispatch => {
  try {
    let allDinerList = await axios.get(
      `/vendor/dinner_list/get_all/${venderId}`
    );
    if (allDinerList.data) {
      dispatch({
        type: DINNER_LIST,
        payload: allDinerList.data
      });
    }
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
  // axios
  //   .get(`/vendor/dinner_list/get_all/${venderId}`)
  //   .then(res => console.log(res))
  //   .catch(err => console.log(err));

  // .then(res => console.log(res));

  // .then(res =>
  //     dispatch({
  //       type: VENDOR_LIST,
  //       payload: res.data
  //     })
};
//.get("/dinner/all_dinner")
