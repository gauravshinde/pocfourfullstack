import axios from "axios";

import { VENDOR_EDIT } from "../types";

//get vendor data

export const getVendoredit = (editUser, history) => dispatch => {
  console.log(editUser);
  axios
    .patch(`/all_vendor/${editUser.id}`, editUser)
    // .patch("/all_vendor/:vendorId", editUser)
    .then(res => history.push("/vendor-list"))
    .catch(err =>
      dispatch({
        type: VENDOR_EDIT,
        payload: err.response.data
      })
    );
};

// .patch(`/all_vendor/${editUser.id}`, editUser)
