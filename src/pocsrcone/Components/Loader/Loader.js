import React, { Component } from "react";

export class Loader extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <h2>Loading...</h2>
          <div id="cooking">
            <div className="bubble" />
            <div className="bubble" />
            <div className="bubble" />
            <div className="bubble" />
            <div className="bubble" />
            <div id="area">
              <div id="sides">
                <div id="pan" />
                <div id="handle" />
              </div>
              <div id="pancake">
                <div id="pastry" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Loader;
