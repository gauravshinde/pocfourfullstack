import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { set_new_waitTime_store } from '../../../pocsrcone/store/actions/statuschangeAction';
import buttonimg from '../../assets/Images/penimg.svg';

export class EditWaitTime extends Component {
  constructor() {
    super();
    this.state = {
      inputTime: ''
      // phone_number: ""
    };
  }

  componentDidMount() {
    this.setState({
      inputTime: this.props.dinner.wait_time ? this.props.dinner.wait_time : 0
      // phone_number: this.props.dinner.phone_number
      //   ? this.props.dinner.phone_number
      //   : this.props.dinner.phone_number
    });
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    let formData = {
      _id: this.props.dinner._id,
      wait_time: this.state.inputTime
    };
    this.props.set_new_waitTime_store(formData);
  };
  render() {
    return (
      <React.Fragment>
        <input
          name='inputTime'
          value={this.state.inputTime}
          onChange={this.onChange}
          className='text-center input-field-css'
        />
        <img src={buttonimg} onClick={this.onSubmit} alt='edit' />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(
  mapStateToProps,
  { set_new_waitTime_store }
)(withRouter(EditWaitTime));
