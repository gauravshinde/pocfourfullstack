import React from 'react';
import ButtonComponent from '../SmallComponents/ButtonComponent';
import { Link } from 'react-router-dom';

const HeaderComponent = ({
  headertext,
  vendorDashboard,
  addVendorBtn,
  editVendorbtn,
  link,
  linktwo,
  linkbutton,
  onClick,
  defaultWaitTime
}) => {
  return (
    <React.Fragment>
      <div className='container-fluid main-header '>
        <div className='row'>
          <div className='divheading'>
            <h4>{headertext}</h4>
            <Link to={link} className='text-decoration-none'>
              <h4>{vendorDashboard ? 'Dashboard' : ''}</h4>
            </Link>
            <div className='d-flex justify-content-between align-items-center'>
              {vendorDashboard === true &&
              addVendorBtn === true &&
              editVendorbtn === true ? (
                <Link
                  to={{
                    pathname: linktwo,
                    state: { defaultWaitTime: defaultWaitTime }
                  }}
                  className='text-decoration-none'
                >
                  <ButtonComponent
                    buttonclass={'add-vendor-button'}
                    buttontext={linkbutton}
                  />
                </Link>
              ) : (
                ''
              )}
              {/* {vendorDashboard ? (
                <img
                  src={require("./../../assets/Images/logout.svg")}
                  alt="logout-img"
                  onClick={onClick}
                />
              ) : (
                ""
              )} */}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default HeaderComponent;
