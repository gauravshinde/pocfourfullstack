import React, { Component } from "react";
import HeaderComponent from "../Header/HeaderComponent";
import LargeText from "../SmallComponents/LargeText";
import InputComponent from "../SmallComponents/InputComponent";
import ButtonComponent from "../SmallComponents/ButtonComponent";
// import TextBoxComponent from "../SmallComponents/TextBoxComponent";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../store/actions/authAction";
import { getVendoredit } from "../../store/actions/vendoreditAction";
// import isEmpty from "./../../store/validation/is-empty";
// import { getVendoredit } from "./../../store/actions/vendoreditAction";

export class EditVendor extends Component {
  constructor() {
    super();
    this.state = {
      address: "",
      email: "",
      phone_number: "",
      username: "",
      vendor_name: "",
      _id: ""
    };
  }

  componentDidMount() {
    // console.log(this.props.location.state.detailid, this.props.vendor);
    if (this.props.vendor.length !== 0) {
      let vendorData = this.props.vendor.find(
        vendor => vendor._id === this.props.location.state.detailid
      );
      /// console.log(vendorData);
      this.setState({
        ...vendorData
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  handleEditVendorChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleEditVendorSubmit = e => {
    e.preventDefault();

    const editUser = {
      vendor_name: this.state.vendor_name,
      email: this.state.email,
      phone_number: this.state.phone_number,
      address: this.state.address,
      username: this.state.username,
      id: this.state._id
    };

    // console.log(editUser);
    this.props.getVendoredit(editUser, this.props.history);
  };

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser(this.props.history);
  };

  render() {
    // const { errors } = this.state;

    // console.log(this.props.location.state.detailid);

    return (
      <React.Fragment>
        <HeaderComponent
          headertext={"Logo"}
          vendorDashboard={true}
          addVendorBtn={false}
          editVendorbtn={false}
          link={"/vendor-list"}
          onClick={this.onLogoutClick}
        />
        <div className="container edit-vendor-align text-center">
          <div className="row">
            <div className="col-sm-2" />
            <div className="col-sm-8">
              <LargeText
                largetext={"Edit Vendor"}
                largetextclass={"editvendor-text mb-3"}
              />
              <form onSubmit={this.handleEditVendorSubmit}>
                <div className="row">
                  <div className="col-sm-6">
                    <InputComponent
                      img={require("./../../assets/Images/usernamepic.svg")}
                      alt={"user-img"}
                      labeltext={"Vendor Name"}
                      name={"vendor_name"}
                      type={"text"}
                      place={"eg. James Bond"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.vendor_name}
                      onChange={this.handleEditVendorChange}
                      // error={errors.vendor_name}
                    />

                    <InputComponent
                      img={require("./../../assets/Images/email-img.svg")}
                      alt={"Email-img"}
                      labeltext={"Email"}
                      name={"email"}
                      type={"email"}
                      place={"eg. something@something.com"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.email}
                      onChange={this.handleEditVendorChange}
                      // error={errors.email}
                    />

                    <InputComponent
                      img={require("./../../assets/Images/phone-img.svg")}
                      alt={"Phone-img"}
                      labeltext={"Phone Number"}
                      name={"phone_number"}
                      type={"Number"}
                      place={"eg. 123567890"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.phone_number}
                      onChange={this.handleEditVendorChange}
                      // error={errors.phone_number}
                    />
                    {/* 
                    <TextBoxComponent
                      img={require("./../../assets/Images/location-img.svg")}
                      name={"address"}
                      value={this.state.address}
                      onChange={this.handleEditVendorChange}
                      // error={errors.address}
                    /> */}
                    <div className="form-group mb-0 text-left username-login-form-group">
                      <label htmlFor="exampleFormControlTextarea1">
                        Address
                      </label>
                      <textarea
                        name={"address"}
                        className="form-control textarea-bottomblack"
                        value={this.state.address}
                        onChange={this.handleEditVendorChange}
                        id="exampleFormControlTextarea1"
                        cols="40"
                        rows="4"
                        placeholder="eg. Riverdale"
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <InputComponent
                      img={require("./../../assets/Images/usernamepic.svg")}
                      alt={"username"}
                      labeltext={"Username"}
                      name={"username"}
                      type={"text"}
                      place={"eg. James Bond"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.username}
                      onChange={this.handleEditVendorChange}
                      // error={errors.username}
                    />

                    {/* <InputComponent
                      img={require("./../../assets/Images/passwordpic.svg")}
                      alt={"password"}
                      labeltext={"Password"}
                      name={"vpassword"}
                      type={"password"}
                      place={"*****"}
                      inputclass={"form-control input-bottomblack"}
                      value={this.state.password}
                      onChange={this.handleEditVendorChange}
                      // error={errors.password}
                    />

                    <InputComponent
                      img={require("./../../assets/Images/passwordpic.svg")}
                      alt={"password"}
                      labeltext={"Confirm Password"}
                      name={"vconfirmpassword"}
                      type={"password"}
                      place={"*****"}
                      inputclass={"form-control input-bottomblack"}
                      value={this.state.password}
                      onChange={this.handleEditVendorChange}
                      // error={errors.confirmpassword}
                    /> */}
                  </div>
                  <div className="col-12">
                    <Link to="/vendor-list">
                      <ButtonComponent
                        buttontype={"button"}
                        buttonclass={"cancel-button mb-4 float-none mr-3"}
                        buttontext={"Cancel"}
                        // handleOnClick={this.handleShowAll}
                      />
                    </Link>

                    <ButtonComponent
                      buttontype={"submit"}
                      buttonclass={"save-button mt-0 mb-4 float-none"}
                      buttontext={"Save"}
                      // handleOnClick={this.handleShowAll}
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className="col-sm-2" />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

EditVendor.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  getVendoredit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  vendor: state.vendor.vendor
  // vendoredit:
  // vendor: state.vendor.vendor
});

export default connect(
  mapStateToProps,
  { logoutUser, getVendoredit }
)(withRouter(EditVendor));
