import React, { Component } from 'react';
import NavbarDashboard from '../NavbarDashboard';

export class MySettings extends Component {
  render() {
    return (
      <React.Fragment>
        <div class='container-fluid'>
          <div className='row'>
            <NavbarDashboard />
          </div>
          <div className='row main-mysetting'>
            <h4>My Settings</h4>
            <div className='col-sm-3'>left</div>
            <div className='col-sm-9'>right</div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MySettings;
