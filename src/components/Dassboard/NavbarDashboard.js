import React, { Component } from 'react';
import DashBoardLink from './DashBoardLink';

export class NavbarDashboard extends Component {
  constructor() {
    super();
    this.state = {
      openNavbar: false
    };
  }

  /****************************
   * @DESC OPEN HAMBURGUR CLICK
   ****************************/
  onClickOpennavbar = () => {
    console.log('hii');
    this.setState({
      openNavbar: !this.state.openNavbar
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className='container-fluid main-dashboard-back' style={{ zIndex:123123213 }}>
          <div className='row'>
            <div className='d-flex'>
              <i
                className='fa fa-bars hamburgur-class'
                aria-hidden='true'
                onClick={this.onClickOpennavbar}
              ></i>
              {this.state.openNavbar ? (
                <DashBoardLink toggler={this.onClickOpennavbar} />
              ) : null}
              <h4>Logo</h4>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default NavbarDashboard;
