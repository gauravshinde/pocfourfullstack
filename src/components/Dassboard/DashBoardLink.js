import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import ButtonComponent from './../../reusableComponents/ButtonComponent';
import Modal from 'react-bootstrap/Modal';
import SlidingComponent from './../../reusableComponents/SlidingComponent';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { logOutUser } from '../../store/actions/authActions';

class DashBoardLink extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false
    };
  }

  /**************************
   * @DESC - MODAL TOOGLER
   ***************************/
  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className='dashboard-menu-slide'>
          <div className='row'>
            <div className='col-12 p-0'>
              <h4 className='float-left'>Menu</h4>
              <img
                src={require('../../assets/images/dashboard/Menu.svg')}
                alt='close'
                className='times-fontawesome float-right'
                onClick={this.props.toggler}
              />
            </div>
            <div className='col-12 restaurant-open'>
              <h5>Restaurant Open</h5>
              <SlidingComponent
                type={'checkbox'}
                toggleclass={'toggle'}
                toggleinputclass={'toggle__switch'}
              />
            </div>
            <div className='col-12 menu-nav'>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='/main-dashboard' activeClassName='selected'>
                  Dashboard
                </NavLink>
              </div>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='' activeClassName='selected'>
                  Ordering
                </NavLink>
              </div>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='/vendordashboard' activeClassName='selected'>
                  Seating
                </NavLink>
              </div>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='' activeClassName='selected'>
                  Reports
                </NavLink>
              </div>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='' activeClassName='selected'>
                  Misc.
                </NavLink>
              </div>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='' activeClassName='selected'>
                  Ratings and Reviews
                </NavLink>
              </div>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='/mysettings' activeClassName='selected'>
                  Settings
                </NavLink>
              </div>
              <div className='total-navlink-div'>
                <div className='empty-div' />
                <NavLink exact to='' activeClassName='selected'>
                  Offers
                </NavLink>
              </div>
            </div>
            <div className='logout-div'>
              <button
                type='button'
                onClick={() => this.props.logOutUser()}
                className='logout-text bg-transparent border-0'
              >
                Logout
              </button>
              <div>
                <img
                  src={require('../../assets/images/dashboard/qr-code.svg')}
                  alt='QR Code'
                  className='img-fluid'
                  onClick={this.modalToggler}
                />
                <Modal
                  show={this.state.modalShow}
                  size='md'
                  onHide={this.modalToggler}
                  centered
                >
                  {/* <Modal.Body className='p-0'> */}
                  <div className='suggest-new-title'>
                    <h3>QR Code</h3>
                  </div>
                  <div className='inside-body-section'>
                    <img
                      src={require('../../assets/images/dashboard/qr-code.svg')}
                      alt='Qr Code'
                      className='img-fluid bg-dark'
                    />
                    <hr />
                    <p className='Qr-code-paragraph'>
                      A QR code (short for "quick response" code) is a type of
                      barcode that contains a matrix of dots. It can be scanned
                      using a QR scanner or a smartphone with built-in camera.
                      Once scanned, software on the device converts the dots
                      within the code into numbers or a string of characters.
                      For example, scanning a QR code with your phone might open
                      a URL in your phone's web browser.
                    </p>
                    {/** */}
                    <div className='w-75 ml-auto d-flex justify-content-between'>
                      <ButtonComponent
                        buttontext='Close'
                        buttontype='button'
                        buttonclass='btn button-main button-white'
                        onClick={this.modalToggler}
                      />
                      <ButtonComponent
                        buttontext='Print'
                        buttontype='button'
                        buttonclass='btn button-main button-orange ml-3'
                        // onClick={this.pageChangeHandle(8)}
                      />
                    </div>
                  </div>
                  {/* </Modal.Body> */}
                </Modal>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(
  null,
  { logOutUser }
)(withRouter(DashBoardLink));
