import React, { Component } from 'react';
import axios from 'axios';
import IconBox from './../../../../reusableComponents/IconBoxButtonComponent';
import { SuggestNew } from './../../../../reusableComponents/SuggestNew';
import isEmpty from './../../../../store/validation/is-Empty';

export class CasualDining extends Component {
  // constructor() {
  //   super();
  //   this.state = {
  //     all_cusines_icons: [],

  //     acceptLimit: '',
  //     distance: '',
  //     placement_period: '',
  //     request_period: '',
  //     selected_cusines_types: [],
  //     primary_selected_icons: {},
  //     uploadMenu: []
  //   };
  // }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        let uploadMenu = this.state.uploadMenu;
        uploadMenu.push(res.data.image_URL);
        this.setState({ uploadMenu: uploadMenu, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  render() {
    return (
      <React.Fragment>
        <div className='row'>
          <div className='col-12'>
            <Services
              state={this.props.state}
              onChange={this.props.onChange}
              onSeatingAreaArraySelector={this.props.onSeatingAreaArraySelector}
              onSelectedAreaPrimarySelector={
                this.props.onSelectedAreaPrimarySelector
              }
            />
          </div>
          <div className='col-12'>
            <UploadMenu
              state={this.props.state}
              onImageUploadHandler={this.props.onImageUploadHandler}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CasualDining;

const Services = ({
  state,
  onChange,
  onSeatingAreaArraySelector,
  onSelectedAreaPrimarySelector
}) => {
  return (
    <div className='cuisine-main'>
      <table style={{ width: '100%' }}>
        <tbody>
          <tr>
            <td className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Auto Accept Limit
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='accept_Limit'
                  value={state.accept_Limit}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
            <td className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Distance
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='distance'
                  value={state.distance}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Mtrs
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Placement Period
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='placement_period'
                  value={state.placement_period}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
            <td className=' px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Request Period
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='request_period'
                  value={state.request_period}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td colSpan='2' className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Where is your seating area?
              </h2>
              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {state.all_seating_area_icons.map((icon, index) => (
                  <IconBox
                    key={index}
                    src={icon.icon}
                    title={icon.title}
                    classsection='main-icon-button'
                    onClick={onSeatingAreaArraySelector(icon)}
                  />
                ))}

                <SuggestNew
                  title='Suggest New'
                  classsection='main-suggest-button'
                />
              </div>

              {/** */}
              {!isEmpty(state.selected_seating_area) ? (
                <h2 className='heading-title' style={{ paddingTop: '30px' }}>
                  Which is your Primary Type?
                </h2>
              ) : null}

              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {state.selected_seating_area.map((icon, index) => (
                  <IconBox
                    key={index}
                    src={icon.icon}
                    title={icon.title}
                    classsection={
                      icon === state.primary_seating_area
                        ? 'main-icon-button_active'
                        : 'main-icon-button'
                    }
                    onClick={onSelectedAreaPrimarySelector(icon)}
                  />
                ))}
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

const UploadMenu = ({ state, onImageUploadHandler }) => {
  return (
    <>
      <div className='cuisine-main col-12'>
        <h2 className='heading-title' style={{ paddingTop: '30px' }}>
          Upload Menu
        </h2>
        <div className='view_chainsBor mb-5' style={{ width: '100%' }}>
          <div className='custom_file_upload'>
            <input
              type='file'
              name='icon'
              id='file'
              onChange={onImageUploadHandler}
              className='custom_input_upload'
            />
            <label className='custom_input_label newChain_add' htmlFor='file'>
              <div>
                <i className='fa fa-plus' style={{ color: '#CCCCCC' }}></i>
              </div>
              <div className='add_new_text'>Add New</div>
            </label>
          </div>

          <div className='newChain_addthree mx-3'>
            {state.uploadMenu.length > 0
              ? state.uploadMenu.map((image, index) => (
                  <img
                    key={index}
                    src={image}
                    className='newChain_addtwo'
                    style={{ height: '100%', width: '100%' }}
                    alt='chain '
                  />
                ))
              : null}
            {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
          </div>
        </div>
      </div>
    </>
  );
};
