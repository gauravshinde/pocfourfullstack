import React, { Component } from 'react';
import axios from 'axios';
import Toggle from './../../../../reusableComponents/SlidingComponent';

export class QSRComponent extends Component {
  // constructor() {
  //   super();
  //   this.state = {
  //     request_period: '',
  //     placement_period: '',
  //     foodItem_counter: false,
  //     takeaway_counter: false,
  //     uploadMenu: []
  //   };
  // }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        let uploadMenu = this.state.uploadMenu;
        uploadMenu.push(res.data.image_URL);
        this.setState({ uploadMenu: uploadMenu, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  render() {
    return (
      <React.Fragment>
        <div className='row'>
          <div className='col-12'>
            <Services
              state={this.props.state}
              onChange={this.props.onChange}
              toggleFunction={this.props.toggleFunction}
            />
          </div>
          <div className='col-12'>
            <UploadMenu
              state={this.props.state}
              onImageUploadHandler={this.props.onImageUploadHandler}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default QSRComponent;

const Services = ({ state, onChange, toggleFunction }) => {
  return (
    <div className='cuisine-main'>
      <table style={{ width: '100%' }}>
        <tbody>
          <tr>
            <td className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Request Period
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='request_period'
                  value={state.request_period}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. Days'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
            <td className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Placement Period
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='placement_period'
                  value={state.placement_period}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. Days'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>

      <div className='col-12'>
        <h2 className='heading-title' style={{ paddingTop: '30px' }}>
          Do you have different counters for different food items?
        </h2>
        <Toggle
          name='foodItem_counter'
          currentState={state.foodItem_counter}
          type={'checkbox'}
          spantext1={'Yes'}
          spantext2={'No'}
          toggleclass={'toggle d-flex align-items-center mb-2'}
          toggleinputclass={'toggle__switch ml-3 mr-3'}
          onChange={toggleFunction}
          defaultChecked={false}
        />

        <h2 className='heading-title' style={{ paddingTop: '30px' }}>
          Do you have a separate counter for takeaway ?
        </h2>
        <Toggle
          name='takeaway_counter'
          currentState={state.takeaway_counter}
          type={'checkbox'}
          spantext1={'Yes'}
          spantext2={'No'}
          toggleclass={'toggle d-flex align-items-center mb-2'}
          toggleinputclass={'toggle__switch ml-3 mr-3'}
          onChange={toggleFunction}
          defaultChecked={false}
        />
      </div>
    </div>
  );
};

const UploadMenu = ({ state, onImageUploadHandler }) => {
  return (
    <>
      <div className='cuisine-main col-12'>
        <h2 className='heading-title' style={{ paddingTop: '30px' }}>
          Upload Menu
        </h2>
        <div className='view_chainsBor mb-5' style={{ width: '100%' }}>
          <div className='custom_file_upload'>
            <input
              type='file'
              name='icon'
              id='file'
              onChange={onImageUploadHandler}
              className='custom_input_upload'
            />
            <label className='custom_input_label newChain_add' htmlFor='file'>
              <div>
                <i className='fa fa-plus' style={{ color: '#CCCCCC' }}></i>
              </div>
              <div className='add_new_text'>Add New</div>
            </label>
          </div>

          <div className='newChain_addthree mx-3'>
            {state.uploadMenu.length > 0
              ? state.uploadMenu.map((image, index) => (
                  <img
                    key={index}
                    src={image}
                    className='newChain_addtwo'
                    style={{ height: '100%', width: '100%' }}
                    alt='chain '
                  />
                ))
              : null}
            {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
          </div>
        </div>
      </div>
    </>
  );
};
