import React, { Component } from 'react';
import axios from 'axios';
import IconBox from './../../../../reusableComponents/IconBoxButtonComponent';
import SuggestNew from './../../../../reusableComponents/SuggestNew';
import Toggle from './../../../../reusableComponents/SlidingComponent';
import isEmpty from './../../../../pocsrcone/store/validation/is-empty';

export class FSRComponent extends Component {
  // constructor() {
  //   super();
  //   this.state = {
  //     acceptLimit: '',
  //     reservation_Limit: '',
  //     around_time: '',
  //     total_seating: '',
  //     placement_period: '',
  //     request_period: '',
  //     facility: false,
  //     highchair_hadicap: false,
  //     uploadMenu: []
  //   };
  // }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        let uploadMenu = this.state.uploadMenu;
        uploadMenu.push(res.data.image_URL);
        this.setState({ uploadMenu: uploadMenu, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  render() {
    return (
      <React.Fragment>
        <div className='row'>
          <div className='col-12'>
            <Services
              state={this.props.state}
              onChange={this.props.onChange}
              toggleFunction={this.props.toggleFunction}
              onSeatingAreaArraySelector={this.props.onSeatingAreaArraySelector}
              onSelectedAreaPrimarySelector={
                this.props.onSelectedAreaPrimarySelector
              }
            />
          </div>
          <div className='col-12'>
            <UploadMenu
              state={this.props.state}
              onImageUploadHandler={this.props.onImageUploadHandler}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default FSRComponent;

const Services = ({
  state,
  onChange,
  toggleFunction,
  onSeatingAreaArraySelector,
  onSelectedAreaPrimarySelector
}) => {
  return (
    <div className='cuisine-main'>
      <table style={{ width: '100%' }}>
        <tbody>
          <tr>
            <td className='w-50 px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Auto Accept Limit
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='accept_Limit'
                  value={state.accept_Limit}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
            <td className='w-50 px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Reservation Time Limit *
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='reservation_Limit'
                  value={state.reservation_Limit}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Mins
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td className='w-50 px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Turn Around Time?
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='around_time'
                  value={state.around_time}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Mins
                </span>
              </div>
            </td>
            <td className='w-50 px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                What is your total seating capacity?
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='total_seating'
                  value={state.total_seating}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Num
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td className='w-50 px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Placement Period
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='placement_period'
                  value={state.placement_period}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
            <td className='w-50 px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Request Period
              </h2>
              <div className='d-flex align-items-center'>
                <input
                  name='request_period'
                  value={state.request_period}
                  onChange={onChange}
                  className='curve_input_field'
                  placeholder='eg. 50'
                />
                <span
                  style={{
                    fontSize: '15px',
                    paddingLeft: '10px',
                    fontFamily: 'AvenirLTStd-Black',
                    color: '#ccc'
                  }}
                >
                  Mtrs
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td colSpan='2'>
              <div className='col-12'>
                <h2 className='heading-title' style={{ paddingTop: '30px' }}>
                  Is this facility inside a hotel?
                </h2>
                <Toggle
                  name='facility'
                  currentState={state.facility}
                  type={'checkbox'}
                  spantext1={'Yes'}
                  spantext2={'No'}
                  toggleclass={'toggle d-flex align-items-center mb-2'}
                  toggleinputclass={'toggle__switch ml-3 mr-3'}
                  onChange={toggleFunction}
                  defaultChecked={false}
                />

                <h2 className='heading-title' style={{ paddingTop: '30px' }}>
                  Would you like to extend the request of highchair and
                  handicap?
                </h2>
                <Toggle
                  name='highchair_hadicap'
                  currentState={state.highchair_hadicap}
                  type={'checkbox'}
                  spantext1={'Yes'}
                  spantext2={'No'}
                  toggleclass={'toggle d-flex align-items-center mb-2'}
                  toggleinputclass={'toggle__switch ml-3 mr-3'}
                  onChange={toggleFunction}
                  defaultChecked={false}
                />
              </div>
            </td>
          </tr>
          <tr>
            <td colSpan='2' className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Where is your seating area?
              </h2>
              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {state.all_seating_area_icons.map((icon, index) => (
                  <IconBox
                    key={index}
                    src={icon.icon}
                    title={icon.title}
                    classsection='main-icon-button'
                    onClick={onSeatingAreaArraySelector(icon)}
                  />
                ))}

                <SuggestNew
                  title='Suggest New'
                  classsection='main-suggest-button'
                />
              </div>

              {/** */}
              {!isEmpty(state.selected_seating_area) ? (
                <h2 className='heading-title' style={{ paddingTop: '30px' }}>
                  Which is your Primary Type?
                </h2>
              ) : null}

              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {state.selected_seating_area.map((icon, index) => (
                  <IconBox
                    key={index}
                    src={icon.icon}
                    title={icon.title}
                    classsection={
                      icon === state.primary_seating_area
                        ? 'main-icon-button_active'
                        : 'main-icon-button'
                    }
                    onClick={onSelectedAreaPrimarySelector(icon)}
                  />
                ))}
              </div>
            </td>
          </tr>
          {/* <tr>
            <td className='px-3'>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Out of the ones you selected, which one is primary?
              </h2>
              <div className='d-flex'>
                <IconBoxButtonComponent
                  title='Indoor'
                  src={require('../../../../assets/images/button-icons/burger.png')}
                  classsection='main-icon-button'
                />
                <IconBoxButtonComponent
                  title='Indoor'
                  src={require('../../../../assets/images/button-icons/burger.png')}
                  classsection='main-icon-button'
                />
              </div>
            </td>
          </tr> */}
        </tbody>
      </table>
    </div>
  );
};

const UploadMenu = ({ state, onImageUploadHandler }) => {
  return (
    <>
      <div className='cuisine-main col-12'>
        <h2 className='heading-title' style={{ paddingTop: '30px' }}>
          Upload Menu
        </h2>
        <div className='view_chainsBor mb-5' style={{ width: '100%' }}>
          <div className='custom_file_upload'>
            <input
              type='file'
              name='icon'
              id='file'
              onChange={onImageUploadHandler}
              className='custom_input_upload'
            />
            <label className='custom_input_label newChain_add' htmlFor='file'>
              <div>
                <i className='fa fa-plus' style={{ color: '#CCCCCC' }}></i>
              </div>
              <div className='add_new_text'>Add New</div>
            </label>
          </div>

          <div className='newChain_addthree mx-3'>
            {state.uploadMenu.length > 0
              ? state.uploadMenu.map((image, index) => (
                  <img
                    key={index}
                    src={image}
                    className='newChain_addtwo'
                    style={{ height: '100%', width: '100%' }}
                    alt='chain '
                  />
                ))
              : null}
            {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
          </div>
        </div>
      </div>
    </>
  );
};
