import React, { Component } from 'react';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';
import CardComponent from '../../../reusableComponents/CardComponent';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';

import QSRComponent from './insidePage/QSRComponent';
import CasualDining from './insidePage/CasualDining';
import FSRComponent from './insidePage/FSRComponent';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { logOutUser } from './../../../store/actions/authActions';
import { getALLICONS } from './../../../store/actions/iconAction';
import { create_new_subscription } from './../../../store/actions/addDetailsActions';

export class SubscriptionDetails extends Component {
  constructor() {
    super();
    this.state = {
      Termsandcondition: false,
      checkboxAcceptTerms: false,
      SignModalShow: false,

      servicePage: 'QSR',
      all_seating_area_icons: [],
      selected_seating_area: [],
      primary_seating_area: {},

      request_period: '',
      placement_period: '',
      foodItem_counter: false,
      takeaway_counter: false,
      accept_Limit: '',
      distance: '',
      seating_area: '',
      reservation_Limit: '',
      around_time: '',
      total_seating: '',
      facility: false,
      highchair_hadicap: false,
      uploadMenu: [],
      status: ''
    };
  }

  componentDidMount() {
    this.props.getALLICONS();
  }

  /****************************************
   * @DESC - Seating Area CODE SELECTOR
   ****************************************/
  onSeatingAreaArraySelector = icon => e => {
    let SeatingArea = this.state.selected_seating_area;
    if (SeatingArea.length === 0) {
      SeatingArea.push(icon);
    } else {
      let isAlreadyPresent = SeatingArea.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = SeatingArea.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          SeatingArea.splice(indexOf, 1);
        }
      } else {
        SeatingArea.push(icon);
      }
    }
    this.setState({
      selected_seating_area: SeatingArea
    });
  };

  onSelectedAreaPrimarySelector = icon => e => {
    this.setState({
      primary_seating_area: icon
    });
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.icons.get_subscription_icon);
    if (
      nextProps.icons.get_subscription_icon !== nextState.all_seating_area_icons
    ) {
      return {
        all_seating_area_icons: nextProps.icons.get_subscription_icon
      };
    }
    return null;
  }

  onClickServicePageChange = value => e => {
    this.setState({
      servicePage: value
    });
  };

  onClickServiceType = data => e => {
    this.setState({
      servicesType: data
    });
  };

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        let uploadMenu = this.state.uploadMenu;
        uploadMenu.push(res.data.image_URL);
        this.setState({ uploadMenu: uploadMenu, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  handleCheckbox = e => {
    this.setState({
      checkboxAcceptTerms: e.target.checked
    });
    console.log(e.target.checked);
  };

  /**************************
   * @DESC - Open Terms and condition Modal Popup
   ***************************/
  termsmodaltoggler = e => {
    e.preventDefault();
    this.setState({
      Termsandcondition: false
      // SignModalShow: this.state.checkboxAcceptTerms ? true : false
    });
  };
  TermsandconditionModalPopup = () => {
    return (
      <React.Fragment>
        <Modal
          show={this.state.Termsandcondition}
          size='xl'
          onHide={this.termsmodaltoggler}
          centered
        >
          <div className='p-0'>
            <div className='term-and-condition-title'>
              <h3>Terms and conditions</h3>
            </div>
            <div className='term-and-condition-widthmain'>
              <div id='style-3' className='w-100 term-and-condition-width'>
                <div className='term-and-condition-para'>
                  <h3>Terms and conditions</h3>
                  <p>
                    General Site Usage
                    <br />
                    Last Revised: December 16,
                    <br />
                    <br />
                    Welcome to Amelio This site is provided as a service to our
                    visitors and may be used for informational purposes only.
                    Because the Terms and Conditions contain legal obligations,
                    please read them carefully.
                    <br />
                    <br />
                  </p>
                  <h3>1. YOUR AGREEMENT</h3>
                  <p>
                    By using this Site, you agree to be bound by, and to comply
                    with, these Terms and Conditions. If you do not agree to
                    these Terms and Conditions, please do not use this site.
                    <br />
                    <br />
                  </p>
                  <span> PLEASE NOTE:</span>
                  <p>
                    We reserve the right, at our sole discretion, to change,
                    modify or otherwise alter these Terms and Conditions at any
                    time. Unless otherwise indicated, amendments will become
                    effective immediately. Please review these Terms and
                    Conditions periodically. Your continued use of the Site
                    following the posting of changes and/or modifications will
                    constitute your acceptance of the revised Terms and
                    Conditions and the reasonableness of these standards for
                    notice of changes. For your information, this page was last
                    updated as of the date at the top of these terms and
                    conditions. 2. PRIVACY Please review our Privacy Policy,
                    which also governs your visit to this Site, to understand
                    our practices. 3. LINKED SITES This Site may contain links
                    to other independent third-party Web sites ("Linked Sites”).
                    These Linked Sites are provided solely as a convenience to
                    our visitors. Such Linked Sites are not under our control,
                    and we are not responsible for and does not endorse the
                    content of such Linked Sites, including any information or
                    materials contained on such Linked Sites. You will need to
                    make your own independent judgment regarding your
                    interaction with these Linked Sites. 4. FORWARD LOOKING
                    STATEMENTS All materials reproduced on this site speak as of
                    the original date of publication or filing. The fact that a
                    document is available on this site does not mean that the
                    information contained in such document has not been modified
                    or superseded by events or by a subsequent document or
                    filing. We have no duty or policy to update any information
                    or statements contained on this site and, therefore, such
                    information or statements should not be relied upon as being
                    current as of the date you access this site. 5. DISCLAIMER
                    OF WARRANTIES AND LIMITATION OF LIABILITY
                  </p>
                </div>
              </div>
              <div className=''>
                <label>
                  <input
                    type='checkbox'
                    name='checkboxAcceptTerms'
                    className='mr-2'
                    onClick={this.handleCheckbox}
                  />
                  I have read the terms and conditions of the agreement.
                </label>
              </div>
              <div
                className='ml-auto mt-4 d-flex justify-content-between'
                style={{ width: '35%' }}
              >
                <ButtonComponent
                  buttontext='Cancel'
                  buttontype='button'
                  buttonclass='btn button-main button-white'
                  onClick={this.termsmodaltoggler}
                />
                <ButtonComponent
                  buttontext='Save'
                  buttontype='button'
                  buttonclass='btn button-main button-orange'
                  onClick={this.modalToggler}
                  disabled={
                    this.state.checkboxAcceptTerms === false ? true : false
                  }
                  // onClick={this.props.pageChanger(8)}
                />
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  };

  /**************************
   * @DESC - Open Modal Popup
   ***************************/
  modalToggler = e => {
    this.setState({
      Termsandcondition: false,
      SignModalShow: !this.state.SignModalShow
    });
  };

  rendreSignOutmodalPopup = () => {
    return (
      <React.Fragment>
        <Modal
          show={this.state.SignModalShow}
          size='md'
          onHide={this.modalToggler}
          centered
        >
          <div className='p-0'>
            <div className='suggest-new-title'>
              <h3>Suggest New</h3>
            </div>
            <div className='inside-body-section'>
              <h3 className='insid-body-headinone'>
                Thank You for Completing Your Profile.
              </h3>
              <p className='insid-body-paragraph'>
                We will get in touch with you by email in the next couple of
                days.
              </p>
              {/** */}
              <div className='float-right mb-3'>
                {/* <ButtonComponent
                  buttontext='Cancel'
                  buttontype='button'
                  buttonclass='btn button-main button-white'
                  onClick={this.modalToggler}
                /> */}
                <ButtonComponent
                  buttontext='Logout'
                  buttontype='submit'
                  buttonclass='btn button-main button-orange ml-3'
                  // onClick={this.pageChangeHandle(8)}
                  onClick={this.props.logOutUser}
                />
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  };

  /**************************
   * @DESC - OnSubmit Handler
   ***************************/
  onSubmit = e => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      all_seating_area_icons: this.state.all_seating_area_icons,
      selected_seating_area: this.state.selected_seating_area,
      primary_seating_area: this.state.primary_seating_area,
      servicePage: this.state.servicePage,
      request_period: this.state.request_period,
      placement_period: this.state.placement_period,
      foodItem_counter: this.state.foodItem_counter,
      takeaway_counter: this.state.takeaway_counter,
      accept_Limit: this.state.accept_Limit,
      distance: this.state.distance,
      seating_area: this.state.seating_area,
      reservation_Limit: this.state.reservation_Limit,
      around_time: this.state.around_time,
      total_seating: this.state.total_seating,
      facility: this.state.facility,
      highchair_hadicap: this.state.highchair_hadicap,
      uploadMenu: this.state.uploadMenu
    };
    this.props.create_new_subscription(formData);
    this.setState({
      Termsandcondition: true,
      checkboxAcceptTerms: false
    });
  };

  renderSubscriptionDetails = () => {
    return (
      <React.Fragment>
        <div className='w-50 ml-auto d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            onClick={this.pageChangeHandle(11)}
          />
          <ButtonComponent
            buttontext='Save'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={this.onSubmit}
            // onClick={this.props.pageChanger(8)}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log(this.state);
    // console.log(this.props.auth.status);
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Subscription Details</h4>
          <p>Select services that you want to be provided by Amaelio.</p>
          <hr className='hr-global' />
          <div className='row'>
            <div className='col-4'>
              <CardComponent
                src='https://chainlist.s3.amazonaws.com/dinner+(1)%402x.png'
                title='QSR'
                des='Curb Side Self Serve Skip the Line Take Away'
                onClick={this.onClickServicePageChange('QSR')}
                classes={
                  this.state.servicePage === 'QSR'
                    ? 'subscription_card_container_active'
                    : 'subscription_card_container'
                }
              />
            </div>
            <div className='col-4'>
              <CardComponent
                des='Walk in WaitList Curb Side Take Away'
                title='Casual Dining'
                src='https://chainlist.s3.amazonaws.com/fast-food%402x.png'
                onClick={this.onClickServicePageChange('casualDining')}
                classes={
                  this.state.servicePage === 'casualDining'
                    ? 'subscription_card_container_active'
                    : 'subscription_card_container'
                }
              />
            </div>
            <div className='col-4'>
              <CardComponent
                des='Curb Side Self Serve Skip the Line Take Away Reservation'
                title='FSR'
                src='https://chainlist.s3.amazonaws.com/restaurant%402x.png'
                onClick={this.onClickServicePageChange('FSR')}
                classes={
                  this.state.servicePage === 'FSR'
                    ? 'subscription_card_container_active'
                    : 'subscription_card_container'
                }
              />
            </div>
          </div>
          <div className='row'>
            <form>
              {this.state.servicePage === 'QSR' ? (
                <QSRComponent
                  state={this.state}
                  onChange={this.onChange}
                  toggleFunction={this.toggleFunction}
                  onImageUploadHandler={this.onImageUploadHandler}
                />
              ) : (
                ''
              )}
              {this.state.servicePage === 'casualDining' ? (
                <CasualDining
                  state={this.state}
                  onChange={this.onChange}
                  onImageUploadHandler={this.onImageUploadHandler}
                  onSeatingAreaArraySelector={this.onSeatingAreaArraySelector}
                  onSelectedAreaPrimarySelector={
                    this.onSelectedAreaPrimarySelector
                  }
                />
              ) : (
                ''
              )}
              {this.state.servicePage === 'FSR' ? (
                <FSRComponent
                  state={this.state}
                  onChange={this.onChange}
                  toggleFunction={this.toggleFunction}
                  onImageUploadHandler={this.onImageUploadHandler}
                  onSeatingAreaArraySelector={this.onSeatingAreaArraySelector}
                  onSelectedAreaPrimarySelector={
                    this.onSelectedAreaPrimarySelector
                  }
                />
              ) : (
                ''
              )}
              {this.TermsandconditionModalPopup()}
              {this.rendreSignOutmodalPopup()}
              {this.renderSubscriptionDetails()}
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  icons: state.icons,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logOutUser, create_new_subscription, getALLICONS }
)(withRouter(SubscriptionDetails));
