import React, { Component } from 'react';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';
import SuggestNew from './../../../reusableComponents/SuggestNew';
import TextAreaComponent from './../../../reusableComponents/TextAreaComponent';
import classnames from 'classnames';
import isEmpty from '../../../store/validation/is-Empty';

import IconBox from '../../../reusableComponents/IconBoxButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { create_new_resto_features } from '../../../store/actions/addDetailsActions';

export class RestaurantFeatures extends Component {
  constructor() {
    super();
    this.state = {
      parking_description: '',
      errors: {},
      all_restaurant_featurs_icons: [],
      get_restaurant_access_icons: [],
      get_parking_icons: [],

      selected_restaurant_features_type: [],
      primary_restaurant_features_type: {},
      selected_restaurant_access_type: [],
      primary_restaurant_access_type: {},
      selected_parking_type: [],
      primary_selecte_parking_type: {}
    };
  }

  onSubmit = e => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      selected_restaurant_features_type: this.state
        .selected_restaurant_features_type,
      primary_restaurant_features_type: this.state
        .primary_restaurant_features_type,
      selected_restaurant_access_type: this.state
        .selected_restaurant_access_type,
      primary_restaurant_access_type: this.state.primary_restaurant_access_type,
      selected_parking_type: this.state.selected_parking_type,
      primary_selecte_parking_type: this.state.primary_selecte_parking_type,
      parking_description: this.state.parking_description
    };
    this.props.create_new_resto_features(formData, this.pageChangeHandle(8));
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      nextProps.icons.get_restrant_features_icons !==
        nextState.all_restaurant_featurs_icons ||
      nextProps.icons.get_restraunt_access_icons !==
        nextState.get_restaurant_access_icons ||
      nextProps.icons.get_parking_icons !== nextState.get_parking_icons
    ) {
      return {
        all_restaurant_featurs_icons:
          nextProps.icons.get_restrant_features_icons,
        get_restaurant_access_icons: nextProps.icons.get_restraunt_access_icons,
        get_parking_icons: nextProps.icons.get_parking_icons
      };
    }
    return null;
  }

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
    if (value === 8) {
      this.props.pageCompletedHandler(3);
    }
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - RESTAURATANT FEATURES
   ****************************************/
  onRestuarantFeatureArraySelector = icon => e => {
    let RestuarantFeature = this.state.selected_restaurant_features_type;
    if (RestuarantFeature.length === 0) {
      RestuarantFeature.push(icon);
    } else {
      let isAlreadyPresent = RestuarantFeature.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = RestuarantFeature.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          RestuarantFeature.splice(indexOf, 1);
        }
      } else {
        RestuarantFeature.push(icon);
      }
    }
    this.setState({
      selected_restaurant_features_type: RestuarantFeature
    });
  };

  onRestuarantFeaturePrimarySelector = icon => e => {
    this.setState({
      primary_restaurant_features_type: icon
    });
  };

  /****************************************
   * @DESC - RESTAURATANT ACCESS
   ****************************************/
  onRestuarantAccessArraySelector = icon => e => {
    let RestuarantAccess = this.state.selected_restaurant_access_type;
    if (RestuarantAccess.length === 0) {
      RestuarantAccess.push(icon);
    } else {
      let isAlreadyPresent = RestuarantAccess.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = RestuarantAccess.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          RestuarantAccess.splice(indexOf, 1);
        }
      } else {
        RestuarantAccess.push(icon);
      }
    }
    this.setState({
      selected_restaurant_access_type: RestuarantAccess
    });
  };

  onRestuarantAccessPrimarySelector = icon => e => {
    this.setState({
      primary_restaurant_access_type: icon
    });
  };

  /****************************************
   * @DESC - PARKING FEATURES
   ****************************************/
  onParkingArraySelector = icon => e => {
    let Parking = this.state.selected_parking_type;
    if (Parking.length === 0) {
      Parking.push(icon);
    } else {
      let isAlreadyPresent = Parking.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = Parking.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          Parking.splice(indexOf, 1);
        }
      } else {
        Parking.push(icon);
      }
    }
    this.setState({
      selected_parking_type: Parking
    });
  };

  onParkingPrimarySelector = icon => e => {
    this.setState({
      primary_selecte_parking_type: icon
    });
  };

  renderRestaurantFeatures = () => {
    const { errors } = this.state;

    return (
      <React.Fragment>
        <div className='cuisine-main'>
          {/** */}
          <h2 className='heading-title'>What features do you provide?</h2>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.all_restaurant_featurs_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection='main-icon-button'
                onClick={this.onRestuarantFeatureArraySelector(icon)}
              />
            ))}
            <SuggestNew
              title='Suggest New'
              classsection='main-suggest-button'
            />
          </div>

          {/** */}
          {!isEmpty(this.state.selected_restaurant_features_type) ? (
            <h2 className='heading-title' style={{ paddingTop: '30px' }}>
              Out of the features selected, which one is primary?
            </h2>
          ) : null}

          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.selected_restaurant_features_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_restaurant_features_type
                    ? 'main-icon-button_active'
                    : 'main-icon-button'
                }
                onClick={this.onRestuarantFeaturePrimarySelector(icon)}
              />
            ))}
          </div>

          {/** */}
          <h2 className='heading-title mt-4'>
            How accessible is your restaurant?
          </h2>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.get_restaurant_access_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection='main-icon-button'
                onClick={this.onRestuarantAccessArraySelector(icon)}
              />
            ))}
            <SuggestNew
              title='Suggest New'
              classsection='main-suggest-button'
            />
          </div>

          {!isEmpty(this.state.selected_restaurant_access_type) ? (
            <h2 className='heading-title' style={{ paddingTop: '30px' }}>
              Which is your primary way of accessing your restaurant?
            </h2>
          ) : null}

          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.selected_restaurant_access_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_restaurant_access_type
                    ? 'main-icon-button_active'
                    : 'main-icon-button'
                }
                onClick={this.onRestuarantAccessPrimarySelector(icon)}
              />
            ))}
          </div>

          {/** */}
          <h2 className='heading-title mt-4'>What parking do you provide?</h2>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.get_parking_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection='main-icon-button'
                onClick={this.onParkingArraySelector(icon)}
              />
            ))}
            <SuggestNew />
          </div>
          {!isEmpty(this.state.selected_parking_type) ? (
            <h2 className='heading-title' style={{ paddingTop: '30px' }}>
              Which is your primary service for parking?
            </h2>
          ) : null}

          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.selected_parking_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_selecte_parking_type
                    ? 'main-icon-button_active'
                    : 'main-icon-button'
                }
                onClick={this.onParkingPrimarySelector(icon)}
              />
            ))}
          </div>
          {/** */}
          <h2 className='heading-title mt-4'>
            description for Parking and Accessibility
          </h2>
          <TextAreaComponent
            inputlabelclass={'d-none'}
            name={'parking_description'}
            type={'text'}
            onChange={this.onChange}
            value={this.state.parking_description}
            place={'eg. if you buy for rs. 1000 you get free parking'}
            error={errors.parking_description}
            Textareaclass={classnames('form-control textarea-custom', {
              invalid: errors.parking_description
            })}
          />
        </div>

        {/** */}
        <div className='w-50 ml-auto d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            onClick={this.pageChangeHandle(6)}
          />
          <ButtonComponent
            buttontext='Next'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Restaurant Features</h4>
          <p>Enter the information about your features in your restaurant</p>
          <hr className='hr-global' />
          <form>{this.renderRestaurantFeatures()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  icons: state.icons,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { create_new_resto_features }
)(withRouter(RestaurantFeatures));
