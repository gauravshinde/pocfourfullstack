import React, { Component } from 'react';
import MainMapYesNo from './mapComponent/MainMapYesNo';
import YesMapSetup from './mapComponent/YesMapSetup';
import NoMapSetup from './mapComponent/NoMapSetup';
import MainPersonOfContact from './PersonOfContact/MainPersonOfContact';
import RestaurantDetails from './RestaurantDetails/RestaurantDetails';
import ServicesDetails from './ServicesDetails/ServicesDetails';
import RestaurantFeatures from './RestaurantFeatures/RestaurantFeatures';
import CuisineFeatures from './CuisineFeatures/CuisineFeatures';
import CuisineFeaturesTwo from './CuisineFeatures/CuisineFeaturesTwo';
import RestaurantTimings from './RestaurantTimings/RestaurantTimings';
import KYCBankInfo from './KYCBankInfo/KYCBankInfo';
import SubscriptionDetails from './SubscriptionDetails/SubscriptionDetails';
import { connect } from 'react-redux';
import { getALLICONS } from '../../store/actions/iconAction';

export class mainDetailPage extends Component {
  constructor() {
    super();
    this.state = {
      pageNo: 1,
      pageView: 1
    };
  }

  componentDidMount() {
    this.props.getALLICONS();
  }
  pageCompletedHandler = value => {
    this.setState({
      pageNo: value
    });
  };

  pageChangeButtonHandler = value => {
    this.setState({
      pageView: value
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className='container-fluid main-details'>
          <div className='row'>
            <div id='style-3' className='col-9 main-details-sectionone'>
              {this.state.pageView === 1 ? (
                <MainMapYesNo pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 2 ? (
                <YesMapSetup pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 3 ? (
                <NoMapSetup pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 4 ? (
                <MainPersonOfContact
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 5 ? (
                <RestaurantDetails
                  pageCompletedHandler={this.pageCompletedHandler}
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 6 ? (
                <ServicesDetails pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 7 ? (
                <RestaurantFeatures
                  pageCompletedHandler={this.pageCompletedHandler}
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 8 ? (
                <CuisineFeatures pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 9 ? (
                <CuisineFeaturesTwo
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 10 ? (
                <RestaurantTimings pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {/* {this.state.pageView === 11 ? (
                <RestaurantTimingsTwo
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 12 ? (
                <RestaurantTimingsThree
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 13 ? (
                <RestaurantTimingsFour
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null} */}
              {this.state.pageView === 11 ? (
                <KYCBankInfo
                  pageCompletedHandler={this.pageCompletedHandler}
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 12 ? (
                <SubscriptionDetails
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
            </div>
            <div className='col-3 main-details-sectiontwo'>
              <h5>Logo</h5>
              <div className='d-flex justify-content-around'>
                <button
                  type='button'
                  className={
                    this.state.pageNo === 1
                      ? 'btn button-without-round button-withRound button-background-select'
                      : 'btn button-without-round'
                  }
                >
                  1
                </button>
                <button
                  type='button'
                  className={
                    this.state.pageNo === 2
                      ? 'btn button-without-round button-withRound button-background-select'
                      : 'btn button-without-round'
                  }
                >
                  2
                </button>
                <button
                  type='button'
                  className={
                    this.state.pageNo === 3
                      ? 'btn button-without-round button-withRound button-background-select'
                      : 'btn button-without-round'
                  }
                >
                  3
                </button>
                <button
                  type='button'
                  className={
                    this.state.pageNo === 4
                      ? 'btn button-without-round button-withRound button-background-select'
                      : 'btn button-without-round '
                  }
                >
                  4
                </button>
              </div>
              <div className='store-setup'>
                <h4>Store Setup</h4>
                <img
                  className='img-fluid'
                  src={require('../../../src/assets/images/maindetails/shop.png')}
                  alt='shop'
                />
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce
                  convallis tellus id malesuada faucibus. Donec dolor mauris.
                </p>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(
  null,
  { getALLICONS }
)(mainDetailPage);
