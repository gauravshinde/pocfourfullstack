import React, { Component } from 'react';
import ButtonComponent from '../../../reusableComponents/ButtonComponent';

export class YesMapSetup extends Component {
  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  render() {
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Map Setup</h4>
          <p>Please enter your Google Maps details.</p>
          <hr className='hr-global' />
          <p>Drop a pin to your address</p>
          <iframe
            title='map'
            src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3784.0380526504996!2d73.89602171436819!3d18.481935475179547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c02aa3a7e783%3A0x4f9e547980873653!2sMyrl%20Tech%20Web%20Development%20Company!5e0!3m2!1sen!2sin!4v1566625734283!5m2!1sen!2sin'
          ></iframe>
        </div>
        <div className='w-50 ml-auto d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            onClick={this.pageChangeHandle(1)}
          />
          <ButtonComponent
            buttontext='Next'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={this.pageChangeHandle(4)}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default YesMapSetup;
