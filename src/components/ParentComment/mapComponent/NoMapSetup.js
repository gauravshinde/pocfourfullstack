import React, { Component } from 'react';
import InputComponent from './../../../reusableComponents/InputComponent';
import classnames from 'classnames';
import TextAreaComponent from '../../../reusableComponents/TextAreaComponent';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { create_new_restaurant } from '../../../store/actions/addDetailsActions';

export class NoMapSetup extends Component {
  constructor() {
    super();
    this.state = {
      restaurant_name: '',
      restaurant_description: '',
      restaurant_area: '',
      restaurant_city: '',
      restaurant_mobile_number: '',
      restaurant_email: '',
      restaurant_address: '',
      errors: {}
    };
  }

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - ONSUBMIT HANDLER
   ***************************/
  onSubmit = e => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user._id,
      is_registered_with_google: false,
      google_place_id: '',
      restaurant_name: this.state.restaurant_name,
      restaurant_description: this.state.restaurant_description,
      restaurant_area: this.state.restaurant_area,
      restaurant_city: this.state.restaurant_city,
      restaurant_mobile_number: this.state.restaurant_mobile_number,
      restaurant_email: this.state.restaurant_email,
      restaurant_address: this.state.restaurant_address
    };
    this.props.create_new_restaurant(formData, this.pageChangeHandle(4));
  };

  renderMapEdit = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <div className='col-10 mx-auto'>
          <InputComponent
            labeltext='Food Establishment Name'
            inputlabelclass='input-label'
            imgbox='d-none'
            name='restaurant_name'
            type='text'
            place='eg. McDonalds'
            onChange={this.onChange}
            value={this.state.restaurant_name}
            error={errors.restaurant_name}
            inputclass={classnames('map-inputfield', {
              invalid: errors.restaurant_name
            })}
          />
          <TextAreaComponent
            labeltext={'Food Establishment Description'}
            inputlabelclass={'input-label'}
            name={'restaurant_description'}
            type={'text'}
            onChange={this.onChange}
            value={this.state.restaurant_description}
            place={'eg. A bright Atmosphere'}
            error={errors.restaurant_description}
            Textareaclass={classnames('form-control textarea-custom', {
              invalid: errors.restaurant_description
            })}
          />
          <div className='row'>
            <div className='col'>
              <InputComponent
                labeltext='Area'
                inputlabelclass='input-label'
                imgbox='d-none'
                name='restaurant_area'
                type='text'
                place='eg. Koregaon Park'
                onChange={this.onChange}
                value={this.state.restaurant_area}
                error={errors.restaurant_area}
                inputclass={classnames('map-inputfield', {
                  invalid: errors.restaurant_area
                })}
              />
            </div>
            <div className='col'>
              <InputComponent
                labeltext='City'
                inputlabelclass='input-label'
                imgbox='d-none'
                name='restaurant_city'
                type='text'
                place='eg. Pune'
                onChange={this.onChange}
                value={this.state.restaurant_city}
                error={errors.restaurant_city}
                inputclass={classnames('map-inputfield', {
                  invalid: errors.restaurant_city
                })}
              />
            </div>
          </div>
          <div className='row'>
            <div className='col'>
              <InputComponent
                labeltext='Phone No. /Landline'
                inputlabelclass='input-label'
                imgbox='d-none'
                name='restaurant_mobile_number'
                type='text'
                place='eg. 9898989898'
                onChange={this.onChange}
                value={this.state.restaurant_mobile_number}
                error={errors.restaurant_mobile_number}
                inputclass={classnames('map-inputfield', {
                  invalid: errors.restaurant_mobile_number
                })}
              />
            </div>
            <div className='col'>
              <InputComponent
                labeltext='Enquiry Email'
                inputlabelclass='input-label'
                imgbox='d-none'
                name='restaurant_email'
                type='email'
                place='eg. admin@gmail.com'
                onChange={this.onChange}
                value={this.state.restaurant_email}
                error={errors.restaurant_email}
                inputclass={classnames('map-inputfield', {
                  invalid: errors.restaurant_email
                })}
              />
            </div>
          </div>
          <TextAreaComponent
            labeltext={'Address'}
            inputlabelclass={'input-label'}
            name={'restaurant_address'}
            type={'text'}
            onChange={this.onChange}
            value={this.state.restaurant_address}
            place={'eg. Riverdale'}
            error={errors.restaurant_address}
            Textareaclass={classnames('form-control textarea-custom', {
              invalid: errors.restaurant_address
            })}
          />
          <p>Drop a pin to your address</p>
          <iframe
            title='map'
            src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3784.0380526504996!2d73.89602171436819!3d18.481935475179547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c02aa3a7e783%3A0x4f9e547980873653!2sMyrl%20Tech%20Web%20Development%20Company!5e0!3m2!1sen!2sin!4v1566625734283!5m2!1sen!2sin'
          ></iframe>
          <div className='col-8 ml-auto mb-4 d-flex justify-content-between'>
            <ButtonComponent
              buttontext='Back'
              buttontype='button'
              buttonclass='btn button-main button-white'
              onClick={this.pageChangeHandle(1)}
            />
            <ButtonComponent
              buttontext='Next'
              buttontype='button'
              buttonclass='btn button-main button-orange'
              onClick={this.onSubmit}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log( this.state );
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Map Setup</h4>
          <p>Please enter your Google Maps details.</p>
          <hr className='hr-global' />
          <form>{this.renderMapEdit()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { create_new_restaurant }
)(withRouter(NoMapSetup));
