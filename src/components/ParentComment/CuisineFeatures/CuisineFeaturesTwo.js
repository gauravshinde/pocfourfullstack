import React, { Component } from 'react';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';
import SuggestNew from './../../../reusableComponents/SuggestNew';
import isEmpty from '../../../store/validation/is-Empty';
import IconBox from '../../../reusableComponents/IconBoxButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { create_cusine_two } from '../../../store/actions/addDetailsActions';

export class CuisineFeaturesTwo extends Component {
  constructor() {
    super();
    this.state = {
      all_cusines_icons: [],

      selected_cusines_types: [],
      primary_selected_icons: {}
    };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      nextProps.icons.get_cusine_types_icons !== nextState.all_cusines_icons
    ) {
      return {
        all_cusines_icons: nextProps.icons.get_cusine_types_icons
      };
    }
    return null;
  }

  onSubmit = e => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      selected_cusines_types: this.state.selected_cusines_types,
      primary_selected_icons: this.state.primary_selected_icons
    };
    this.props.create_cusine_two(formData, this.pageChangeHandle(10));
  };

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - CUSINE SELECTOR
   ****************************************/
  onCusineSelectorArraySelector = icon => e => {
    let Cusine = this.state.selected_cusines_types;
    if (Cusine.length === 0) {
      Cusine.push(icon);
    } else {
      let isAlreadyPresent = Cusine.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = Cusine.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          Cusine.splice(indexOf, 1);
        }
      } else {
        Cusine.push(icon);
      }
    }
    this.setState({
      selected_cusines_types: Cusine
    });
  };

  onCusinesPrimarySelector = icon => e => {
    this.setState({
      primary_selected_icons: icon
    });
  };

  renderCuisineFeaturesTwo = () => {
    return (
      <React.Fragment>
        <div className='cuisine-main'>
          <h2 className='heading-title' style={{ paddingTop: '30px' }}>
            What type of cuisine do you serve? (Multiple Selection)
          </h2>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.all_cusines_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection='main-icon-button'
                onClick={this.onCusineSelectorArraySelector(icon)}
              />
            ))}
            <SuggestNew
              title='Suggest New'
              classsection='main-suggest-button'
            />
          </div>

          {/** */}
          {!isEmpty(this.state.selected_cusines_types) ? (
            <h2 className='heading-title' style={{ paddingTop: '30px' }}>
              Out of the cuisines you selected, which one is primary?
            </h2>
          ) : null}

          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.selected_cusines_types.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_selected_icons
                    ? 'main-icon-button_active'
                    : 'main-icon-button'
                }
                onClick={this.onCusinesPrimarySelector(icon)}
              />
            ))}
          </div>
        </div>

        {/* <hr className='hr-global mt-5 mb-2' /> */}
        <div className=' mt-2 w-50 ml-auto mt-5 d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            onClick={this.pageChangeHandle(8)}
          />
          <ButtonComponent
            buttontext='Next'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Cuisine Features</h4>
          <p>
            Enter the information about the cuisines provided in your
            restaurant.
          </p>
          <hr className='hr-global' />
          <form>{this.renderCuisineFeaturesTwo()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  icons: state.icons,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { create_cusine_two }
)(withRouter(CuisineFeaturesTwo));
