import React, { Component } from 'react';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  create_new_poc,
  delete_poc_contact_details
} from '../../../store/actions/addDetailsActions';

export class MainPersonOfContact extends Component {
  constructor() {
    super();
    this.state = {
      fname: '',
      lname: '',
      email: '',
      position: '',
      phone_numbers: [{ id: 1, phone: '', country_code: '' }],
      contacts_array: [],
      errors: {},
      editKey: ''
    };
  }

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onPhoneNumberAddHandler = e => {
    let phone_numbers = this.state.phone_numbers;
    let id = phone_numbers[phone_numbers.length - 1].id;
    let newData = { id: id + 1, phone: '', country_code: '' };
    phone_numbers.push(newData);
    this.setState({
      phone_numbers: phone_numbers
    });
  };

  onPhoneChangeHandler = id => e => {
    let phone_numbers = this.state.phone_numbers;
    let phoneObj = phone_numbers.find(phone => phone.id === id);
    phoneObj.phone = e.target.value;
    this.setState({
      phone_numbers: phone_numbers
    });
  };

  onPhoneNumberDeleteHandler = id => e => {
    let phone_numbers = this.state.phone_numbers;
    if (phone_numbers.length === 1) {
      window.alert('Cannot Delete elements');
    } else {
      let phoneObj = phone_numbers.find(phone => phone.id === id);
      if (phoneObj === 0 || phoneObj) {
        phone_numbers.splice(phone_numbers.indexOf(phoneObj), 1);
        this.setState({ phone_numbers: phone_numbers });
      }
    }
  };

  onAddMoreContactHandler = e => {
    let contacts_array = this.state.contacts_array;
    if (contacts_array.length === 0) {
      let contactData = {
        id: 1,
        fname: this.state.fname,
        lname: this.state.lname,
        email: this.state.email,
        position: this.state.position,
        phone_numbers: this.state.phone_numbers
      };
      contacts_array.push(contactData);
      this.setState({
        contacts_array: contacts_array,
        fname: '',
        lname: '',
        email: '',
        position: '',
        phone_numbers: [{ id: 1, phone: '', country_code: '' }]
      });
    } else {
      let id = contacts_array[contacts_array.length - 1].id;
      let contactData = {
        id: id + 1,
        fname: this.state.fname,
        lname: this.state.lname,
        email: this.state.email,
        position: this.state.position,
        phone_numbers: this.state.phone_numbers
      };
      contacts_array.push(contactData);
      this.setState({
        contacts_array: contacts_array,
        fname: '',
        lname: '',
        email: '',
        position: '',
        phone_numbers: [{ id: 1, phone: '', country_code: '' }]
      });
    }
  };

  /******************************
   * ONSUBMIT HANDLER
   ******************************/
  onSubmitHandler = () => {
    if (this.state.fname && this.state.lname) {
      this.onAddMoreContactHandler();
    }
    let formData = {
      vendor_id: this.props.auth.user._id,
      contacts_array: this.state.contacts_array
    };
    this.props.create_new_poc(formData, this.pageChangeHandle(5));
  };
  /******************************
   * ONEDIT BUTTON HANDLER
   ******************************/
  onEditButtonCLickHandler = id => e => {
    let contacts_array = this.state.contacts_array;
    let phoneObj = contacts_array.find(phone => phone.id === id);
    let indexOf = contacts_array.indexOf(phoneObj);
    this.setState({
      editKey: id,
      fname: contacts_array[indexOf].fname,
      lname: contacts_array[indexOf].lname,
      email: contacts_array[indexOf].email,
      position: contacts_array[indexOf].position,
      phone_numbers: contacts_array[indexOf].phone_numbers
    });
  };
  /******************************
   * ONEDIT SAVE HANDLER
   ******************************/
  onEditSaveHandler = e => {
    let contacts_array = this.state.contacts_array;
    let phoneObj = contacts_array.find(
      phone => phone.id === this.state.editKey
    );
    let indexOf = contacts_array.indexOf(phoneObj);
    contacts_array[indexOf].fname = this.state.fname;
    contacts_array[indexOf].lname = this.state.lname;
    contacts_array[indexOf].email = this.state.email;
    contacts_array[indexOf].position = this.state.position;
    contacts_array[indexOf].phone_numbers = this.state.phone_numbers;
    // console.log( contacts_array );
    this.setState({
      contacts_array: contacts_array,
      editKey: '',
      fname: '',
      lname: '',
      email: '',
      position: '',
      phone_numbers: [{ id: 1, phone: '', country_code: '' }]
    });
  };

  /******************************
   * ONCLICK DELETE HANDLER
   ******************************/
  onClickContactDelete = event => {
    event.preventDefault();
    console.log('clicked');
    let deleteData = {
      _id: this.state.contacts_array,
      email: this.state.email
    };
    this.props.delete_poc_contact_details(deleteData);
  };

  render() {
    // console.log( this.state );
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Person of Contact</h4>
          <p>Enter the information for the Location Manager</p>
          <hr className='hr-global' />
          <form>
            <RenderPocForm
              state={this.state}
              onChange={this.onChange}
              errors={this.state.errors}
              onPhoneNumberAddHandler={this.onPhoneNumberAddHandler}
              onPhoneNumberDeleteHandler={this.onPhoneNumberDeleteHandler}
              onPhoneChangeHandler={this.onPhoneChangeHandler}
              onAddMoreContactHandler={this.onAddMoreContactHandler}
              onEditButtonCLickHandler={this.onEditButtonCLickHandler}
              onClickContactDelete={this.onClickContactDelete}
            />
          </form>
          {renderButtons(
            this.pageChangeHandle,
            this.onSubmitHandler,
            this.state,
            this.onEditSaveHandler
          )}

          {this.state.contacts_array.length > 0 ? (
            <RenderAddedContacts
              contacts_array={this.state.contacts_array}
              onEditButtonCLickHandler={this.onEditButtonCLickHandler}
              onClickContactDelete={this.onClickContactDelete}
              editbutton={true}
            />
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { create_new_poc, delete_poc_contact_details }
)(withRouter(MainPersonOfContact));

const renderButtons = (
  pageChangeHandle,
  onSubmitHandler,
  state,
  onEditSaveHandler
) => {
  return (
    <React.Fragment>
      <div className='w-50 mx-auto mt-4 d-flex justify-content-between'>
        <ButtonComponent
          buttontext='Back'
          buttontype='button'
          buttonclass='btn button-main button-white'
          onClick={pageChangeHandle(3)}
        />
        {!state.editKey ? (
          <ButtonComponent
            buttontext='Next'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={() => onSubmitHandler()}
          />
        ) : (
          <ButtonComponent
            buttontext='Save'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={onEditSaveHandler}
          />
        )}
      </div>
    </React.Fragment>
  );
};

const RenderPocForm = ({
  state,
  onChange,
  errors,
  onPhoneNumberAddHandler,
  onPhoneNumberDeleteHandler,
  onPhoneChangeHandler,
  onAddMoreContactHandler
}) => {
  return (
    <>
      <div className='poc_add_another'>
        <ButtonComponent
          buttontext='Add Another Contact'
          buttontype='button'
          buttonclass='btn button-main button-white'
          onClick={onAddMoreContactHandler}
        />
      </div>
      <div className='form_container_poc'>
        <div className='poc_line_one'>
          <table style={{ width: '100%', borderRadius: '0px' }}>
            <tbody>
              <tr>
                <td className='pr-2 poc_td'>
                  <div className='poc_label'>First Name</div>
                  <input
                    type='text'
                    name='fname'
                    value={state.fname}
                    onChange={onChange}
                    placeholder='eg. James'
                    className='poc_input_field'
                  />
                  {errors.fname ? (
                    <div className='error'>{errors.fname}</div>
                  ) : null}
                </td>
                <td className='pl-2 poc_td'>
                  <div className='poc_label'>Last Name</div>
                  <input
                    type='text'
                    name='lname'
                    value={state.lname}
                    onChange={onChange}
                    placeholder='eg. Bond'
                    className='poc_input_field'
                  />
                  {errors.lname ? (
                    <div className='error'>{errors.lname}</div>
                  ) : null}
                </td>
              </tr>

              <tr>
                <td colSpan='2' className='poc_td'>
                  <div className='poc_label'>Email</div>
                  <input
                    type='text'
                    name='email'
                    value={state.email}
                    onChange={onChange}
                    placeholder='eg. james@bond.com'
                    className='poc_input_field'
                  />
                  {errors.email ? (
                    <div className='error'>{errors.email}</div>
                  ) : null}
                </td>
              </tr>

              <tr>
                <td colSpan='2' className='poc_td'>
                  <div className='poc_label'>Position</div>
                  <input
                    type='text'
                    name='position'
                    value={state.position}
                    onChange={onChange}
                    placeholder='eg. james@bond.com'
                    className='poc_input_field'
                  />
                  {errors.position ? (
                    <div className='error'>{errors.position}</div>
                  ) : null}
                </td>
              </tr>

              <tr>
                <td colSpan='2' className='poc_td'>
                  <div className='poc_label'>Phone Number</div>
                </td>
              </tr>
            </tbody>
          </table>
          <table style={{ width: '100%', borderRadius: '0px' }}>
            <tbody>
              {state.phone_numbers.map((phone, index) => (
                <tr key={index}>
                  <td style={{ width: '30%' }} className='poc_td pr-2'>
                    <select className='poc_input_field'>
                      <option>+91</option>
                    </select>
                  </td>
                  <td className='poc_td pl-2'>
                    <input
                      type='text'
                      name='position'
                      value={phone.phone}
                      onChange={onPhoneChangeHandler(phone.id)}
                      placeholder='eg. 912131232'
                      className='poc_input_field'
                    />
                  </td>
                  <td className='pl-2'>
                    <i
                      onClick={onPhoneNumberDeleteHandler(phone.id)}
                      className='fa fa-trash'
                      style={{ color: 'red', cursor: 'pointer' }}
                      aria-hidden='true'
                    ></i>
                  </td>
                </tr>
              ))}

              <tr onClick={onPhoneNumberAddHandler}>
                <td className='poc_td pr-2'>
                  <div className='poc_input_field_add'>+91</div>
                </td>
                <td className='poc_td pl-2'>
                  <div className='poc_input_field_add'>eg. 987654567</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export const RenderAddedContacts = ({
  contacts_array,
  onEditButtonCLickHandler,
  editbutton,
  deletebutton,
  onClickContactDelete
}) => {
  return (
    <div className='table_contact_container'>
      <div className='added_contacts'>Added Contacts</div>
      <table style={{ width: '100%' }}>
        <tbody>
          <tr>
            <td className='td_head'>Name</td>
            <td style={{ width: '30%' }} className='td_head'>
              Email
            </td>
            <td style={{ width: '20%' }} className='td_head'>
              Position
            </td>
            <td style={{ width: '20%' }} className='td_head'>
              Number
            </td>
            <td style={{ width: '10%' }} className='td_head'>
              Edit
            </td>
          </tr>
          {contacts_array.map((contact, index) => (
            <tr key={index}>
              <td className='td_body'>
                {contact.fname}&emsp;{contact.lname}
              </td>
              <td className='td_body'>{contact.email}</td>
              <td className='td_body'>{contact.position}</td>
              <td>
                {contact.phone_numbers.map((phone, index) => (
                  <div style={{ fontSize: '19px' }} key={index}>
                    {phone.phone}
                  </div>
                ))}
              </td>
              <td className='td_body'>
                {editbutton === true ? (
                  <i
                    onClick={onEditButtonCLickHandler(contact.id)}
                    className='fa fa-edit'
                    style={{ color: 'red' }}
                    aria-hidden='true'
                  ></i>
                ) : null}
                {deletebutton === true ? (
                  <i
                    onClick={onClickContactDelete(contact._id)}
                    className='fa fa-trash'
                    style={{ color: 'red' }}
                    aria-hidden='true'
                  ></i>
                ) : null}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
