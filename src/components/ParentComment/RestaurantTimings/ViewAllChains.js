import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import photo from '../../../assets/images/maindetails/Group.png';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';

export class ViewAllChains extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
      allImage: []
    };
  }

  /*************************
   * @DESC - MODAL TOGGLER
   *************************/
  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className='seleect_chain' onClick={this.modalToggler}>
          View all
        </div>
        <Modal
          show={this.state.modalShow}
          size='xl'
          onHide={this.modalToggler}
          centered
        >
          <Modal.Body className='p-0'>
            <div className='suggest-new-title'>
              <h3>Chains</h3>
            </div>
            <div className='inside-body-section chain-display-image'>
              <h3>Select a few Chains</h3>
              <div className='w-100 overflow-auto' style={{ height: '50vh' }}>
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
                <img src={photo} alt='chain' className='img-fluid' />
              </div>
            </div>
            <div
              className='col-12 pt-4 pb-4 ml-auto d-flex justify-content-between'
              style={{ width: '35%' }}
            >
              <ButtonComponent
                buttontext='Back'
                buttontype='button'
                buttonclass='btn button-main button-white'
                onClick={this.modalToggler}
              />
              <ButtonComponent
                buttontext='Next'
                buttontype='button'
                buttonclass='btn button-main button-orange'
                onClick={this.onSubmit}
              />
            </div>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

export default ViewAllChains;
