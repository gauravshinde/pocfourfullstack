import React, { Component } from 'react';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';
import SuggestNew from './../../../reusableComponents/SuggestNew';
import isEmpty from '../../../store/validation/is-Empty';
import IconBox from '../../../reusableComponents/IconBoxButtonComponent';
import Toggle from '../../../reusableComponents/SlidingComponent';

import AddNewChain from './AddNewChain';
import ViewAllChains from './ViewAllChains';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { create_restaraunttime } from './../../../store/actions/addDetailsActions';
import SlidingComponent from './../../../reusableComponents/SlidingComponent';



const seating_area = [{ title:"Indoor"}, { title:"Outdoor"}, { title:"Private"}, { title:"Bar"}];

const foodTime = [
  "breakfast",
  "lunch",
  "dinner"
]

const dayArray=[
  "monday",
  "tuesday",
  "wednesday",
  "thrusday",
  "friday",
  "saturday",
  "sunday"
]

const timeArray = [
  "00:00",
  "01:00",
  "02:00",
  "03:00",
  "04:00",
  "05:00",
  "06:00",
  "07:00",
  "08:00",
  "09:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00",
  "20:00",
  "21:00",
  "22:00",
  "23:00",
  "24:00",
]

export class RestaurantTimings extends Component {
  constructor() {
    super();
    this.state = {
        all_seating_area_icons:seating_area,
        selected_seating_area:[],
        primary_seating_area:{},
        total_seating_capacity:'',
        part_of_chain:false,
        selectedChains:[],

        are_you_open_24_x_7 : true,
        multiple_opening_time : false,
        table_management: false,

        monday:{
          open:false,
          main_opening_time:'',
          main_closing_time:"",
          breakfast:{
            open : false,
            breakfast_opening_time:'',
            breakfast_closing_time:''
          },
          lunch:{
            open : false,
            lunch_opening_time:'',
            lunch_closing_time:''
          },
          dinner:{
            open:false,
            dinner_opening_time:"",
            dinner_closing_time:''
          }
        }
        ,
        tuesday:{
          open:false,
          main_opening_time:'',
          main_closing_time:"",
          breakfast:{
            open : false,
            breakfast_opening_time:'',
            breakfast_closing_time:''
          },
          lunch:{
            open : false,
            lunch_opening_time:'',
            lunch_closing_time:''
          },
          dinner:{
            open:false,
            dinner_opening_time:"",
            dinner_closing_time:''
          }
        },
        wednesday:{
          open:false,
          main_opening_time:'',
          main_closing_time:"",
          breakfast:{
            open : false,
            breakfast_opening_time:'',
            breakfast_closing_time:''
          },
          lunch:{
            open : false,
            lunch_opening_time:'',
            lunch_closing_time:''
          },
          dinner:{
            open:false,
            dinner_opening_time:"",
            dinner_closing_time:''
          }
        },
        thrusday:{
          open:false,
          main_opening_time:'',
          main_closing_time:"",
          breakfast:{
            open : false,
            breakfast_opening_time:'',
            breakfast_closing_time:''
          },
          lunch:{
            open : false,
            lunch_opening_time:'',
            lunch_closing_time:''
          },
          dinner:{
            open:false,
            dinner_opening_time:"",
            dinner_closing_time:''
          }
        },
        friday:{
          open:false,
          main_opening_time:'',
          main_closing_time:"",
          breakfast:{
            open : false,
            breakfast_opening_time:'',
            breakfast_closing_time:''
          },
          lunch:{
            open : false,
            lunch_opening_time:'',
            lunch_closing_time:''
          },
          dinner:{
            open:false,
            dinner_opening_time:"",
            dinner_closing_time:''
          }
        },
        saturday:{
          open:false,
          main_opening_time:'',
          main_closing_time:"",
          breakfast:{
            open : false,
            breakfast_opening_time:'',
            breakfast_closing_time:''
          },
          lunch:{
            open : false,
            lunch_opening_time:'',
            lunch_closing_time:''
          },
          dinner:{
            open:false,
            dinner_opening_time:"",
            dinner_closing_time:''
          }
        }
        ,        
        sunday:{
          open:false,
          main_opening_time:'',
          main_closing_time:"",
          breakfast:{
            open : false,
            opening_time:'',
            closing_time:''
          },
          lunch:{
            open : false,
            opening_time:'',
            closing_time:''
          },
          dinner:{
            open:false,
            opening_time:"",
            closing_time:''
          }
        }
    };
  }

  /***********************
   * @DESC - PAGE CHANGER
   **********************/
  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - TOGGLE FUNCTION
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /****************************
   * @DESC - DAY TOGGLER -
   ***************************/
  onDayOpenCloseHanlder = day => e => {
    let state = this.state;
    let dayData = state[day];
    dayData.open = !dayData.open;
    this.setState({
      state : state
    });
  }

  onTimeSelectHandlerSingle = day => e => {
    let state = this.state;
    let dayData = state[day];
    dayData[e.target.name] = e.target.value;
    this.setState({
      state : state
    })
  }

  onFoodTypeOpenClose = ( day, food ) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData.open = ! foodTimeData.open;
    this.setState({
      state : state
    })
  }

  onTimeSelectHandlerMultiple = (day, food) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData[e.target.name] = e.target.value;
    this.setState({
      state : state
    })
  }

    /****************************************
   * @DESC - Seating Area CODE SELECTOR
   ****************************************/
  onSeatingAreaArraySelector = (icon) => e => {
    let SeatingArea = this.state.selected_seating_area;
    if( SeatingArea.length === 0 ){
      SeatingArea.push( icon );
    } else {
      let isAlreadyPresent = SeatingArea.includes( icon );
      if( isAlreadyPresent ){
        let indexOf = SeatingArea.indexOf( icon );
        if( indexOf === 0 || indexOf ){
          SeatingArea.splice( indexOf, 1 );
        } 
      }
      else {
        SeatingArea.push( icon );
      }
    }
    this.setState({
      selected_seating_area: SeatingArea
    })
  }

  onSelectedAreaPrimarySelector = ( icon ) => e => {
    this.setState({
      primary_seating_area: icon
    })
  }

  /**************************
   * @DESC - OnSubmit Handler
   ***************************/
  onSubmit = e => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      all_seating_area_icons: this.state.all_seating_area_icons,
      part_of_chain: this.state.part_of_chain,
      selectedChains: this.state.selectedChains,
      are_you_open_24_x_7: this.state.are_you_open_24_x_7,
      multiple_opening_time: this.state.multiple_opening_time,
      table_management: this.state.table_management,
      monday: this.state.monday,
      tuesday: this.state.tuesday,
      wednesday: this.state.wednesday,
      thrusday: this.state.thrusday,
      friday: this.state.friday,
      saturday: this.state.saturday,
      sunday: this.state.sunday,
      selected_seating_area: this.state.selected_seating_area,
      primary_seating_area: this.state.primary_seating_area,
      total_seating_capacity: this.state.total_seating_capacity
    };
    this.props.create_restaraunttime(formData, this.pageChangeHandle(11));
  };



  renderRestaurantTimings = () => {
    return (
      <React.Fragment>
        {/* <hr className='hr-global mt-5 mb-2' /> */}
        <div className='w-50 ml-auto mt-5 d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            onClick={this.pageChangeHandle(9)}
          />
          <ButtonComponent
            buttontext='Next'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log( this.state );
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Restaurant Timings</h4>
          <p>Enter the Details about the Locale of your restaurant</p>
          <hr className='hr-global' />
          <form>
              <PartOfChain state={ this.state } toggleFunction={ this.toggleFunction } />
              <Restauranttiming state={!isEmpty(this.state) &&  this.state } toggleFunction={ this.toggleFunction } />
              { !this.state.multiple_opening_time && !this.state.are_you_open_24_x_7 ? <NoMultipleTime  state={ this.state } toggleFunction={ this.toggleFunction } onDayOpenCloseHanlder={ this.onDayOpenCloseHanlder } onTimeSelectHandlerSingle={ this.onTimeSelectHandlerSingle } /> : null }
              { this.state.multiple_opening_time && !this.state.are_you_open_24_x_7 ? 
              <YesMultipleTime state={ this.state }
                onDayOpenCloseHanlder={ this.onDayOpenCloseHanlder }
                onFoodTypeOpenClose={ this.onFoodTypeOpenClose }
                onTimeSelectHandlerMultiple={ this.onTimeSelectHandlerMultiple }
              /> : null }
              <SeatingArrangement
                 state={ this.state }
                 onSeatingAreaArraySelector={ this.onSeatingAreaArraySelector }
                 onSelectedAreaPrimarySelector={ this.onSelectedAreaPrimarySelector }  
                 onChange={ this.onChange } 
                 toggleFunction={ this.toggleFunction }
              />
              {this.renderRestaurantTimings()}
          </form>
        </div>
      </React.Fragment>
    );
  }
}


const mapStateToProps = state => ({
  icons: state.icons,
  auth: state.auth
});

export default connect( mapStateToProps, {create_restaraunttime} )(withRouter( RestaurantTimings ));


const PartOfChain = ({ state, toggleFunction }) => {
  return (
    <>
      <div className='cuisine-main'>
        <h2 className='heading-title' style={{ paddingTop:'30px' }}>Are you a part of chain ?</h2>
        <Toggle
          name='part_of_chain'
          currentState={state.part_of_chain}
          type={'checkbox'}
          spantext1={'Yes'}
          spantext2={'No'}
          toggleclass={'toggle d-flex align-items-center mb-2'}
          toggleinputclass={'toggle__switch ml-3 mr-3'}
          onChange={toggleFunction}
          defaultChecked={false}
        />
      </div>
      {
        state.part_of_chain ? 
        <div className='cuisine-main mt-4'>
          <table style={{ width:'100%', borderRadius: '0px' }}>
            <tbody>
              <tr>
                <td className="seleect_chain"> Select Chain </td>
                <td style={{ width:'10%' }} ><ViewAllChains/></td>
              </tr>
            </tbody>
          </table>

            <div className='view_chainsBor'>
              <AddNewChain/>
            </div>
        </div>
        : null
      }
    </>
  )
}

const Restauranttiming = ({ state, toggleFunction }) => {
  return (
    <>
      <div className='cuisine-main'>
        <h2 className='heading-title' style={{ paddingTop:'30px' }}>Enter your restaurant timings ?</h2>
        <h2 className='heading-title' >Are you open 24 x 7 ?</h2>
        <Toggle
          name='are_you_open_24_x_7'
          currentState={state.are_you_open_24_x_7}
          type={'checkbox'}
          spantext1={'Yes'}
          spantext2={'No'}
          toggleclass={'toggle d-flex align-items-center mb-2'}
          toggleinputclass={'toggle__switch ml-3 mr-3'}
          onChange={toggleFunction}
          defaultChecked={true}
        />

        { !state.are_you_open_24_x_7 ? <h2 className='heading-title' style={{ paddingTop:'30px' }}>Do you have multiple opening timings ?</h2> : null }
        {
          !state.are_you_open_24_x_7 ? 
            <Toggle
              name='multiple_opening_time'
              currentState={state.multiple_opening_time}
              type={'checkbox'}
              spantext1={'Yes'}
              spantext2={'No'}
              toggleclass={'toggle d-flex align-items-center mb-2'}
              toggleinputclass={'toggle__switch ml-3 mr-3'}
              onChange={toggleFunction}
              defaultChecked={false}
          /> : null
        }
      </div>
    </>
  )
}


const NoMultipleTime = ({ state , toggleFunction, onDayOpenCloseHanlder, onTimeSelectHandlerSingle }) => {
  return (
    <>
      <div className='cuisine-main'>
        {
          dayArray.map( ( day, index ) => (
            <table key={ index } style={{ width : '100%', borderRadius: '0px' }} >
            <tbody>
              <tr>
                <td>
                  <h2 className='heading-title text-capitalize'style={{ fontSize:'15px' }}>{day}</h2>
                  <Toggle
                    name={ day }
                    currentState={state[day].open}
                    type={'checkbox'}
                    spantext1={'Yes'}
                    spantext2={'No'}
                    toggleclass={'toggle d-flex align-items-center mb-2'}
                    toggleinputclass={'toggle__switch ml-3 mr-3'}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={false}
                    />
                </td>
                <td style={{ width : "30%" }}>
                  {/* OPEN AND CLOSE TIME */}
                  <h2 className='heading-title'>Opening Time</h2>
                  <div className='opening_time_selector'>
  
                    <select name="main_opening_time"  onChange={ onTimeSelectHandlerSingle(day) } className="Selection_box">
                      {timeArray.map( ( time, index ) => (<option key={index} value={ time }>{ time }</option>) )}
                    </select>
                    <div className="dasheds">-</div>
                    <select name="main_closing_time"  onChange={ onTimeSelectHandlerSingle(day) } className="Selection_box">
                      {timeArray.map( ( time, index ) => (<option key={index} value={ time }>{ time }</option>) )}
                    </select>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          ))
        }
      </div>
    </>
  )
}


const YesMultipleTime = ({ state, onDayOpenCloseHanlder, onFoodTypeOpenClose, onTimeSelectHandlerMultiple  }) => {
  return(
    <>
      <div className='cuisine-main'>
        {
          dayArray.map( ( day, index ) => (
            <table key={ index } style={{ width : '100%' }} >
            <tbody>
              <tr>
                <td style={{width: '18%'}}>
                  <h2 className='heading-title text-capitalize' style={{ fontSize:'15px' }} >{day}</h2>
                  <Toggle
                    name={ day }
                    currentState={state[day].open}
                    type={'checkbox'}
                    spantext1={'Yes'}
                    spantext2={'No'}
                    toggleclass={'toggle d-flex align-items-center mb-2'}
                    toggleinputclass={'toggle__switch ml-3 mr-3'}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={false}
                    />
                </td>
                {
                  foodTime.map( ( food, index ) => (
                    <td  key={ index }  style={{ width : "200px", paddingLeft:'20px' ,padding:'15px' }}>
                    {/* OPEN AND CLOSE TIME */}
                    <h2 className='heading-title text-capitalize d-flex justify-content-between' style={{ fontSize:'15px' }}>{ food }
                    <Toggle
                    name={ day }
                    currentState={state[day].open}
                    type={'checkbox'}
                    spantext1={''}
                    spantext2={''}
                    toggleclass={'toggle d-flex align-items-center mb-2'}
                    toggleinputclass={'toggle__switch ml-3 mr-3'}
                    onChange={onFoodTypeOpenClose(day, food)}
                    defaultChecked={false}
                    />
                    </h2>
                    <div className='opening_time_selector'>
    
                      <select name="opening_time"  onChange={ onTimeSelectHandlerMultiple(day, food) } className="Selection_box">
                        {timeArray.map( ( time, index ) => (<option key={index} value={ time }>{ time }</option>) )}
                      </select>
                      <div className="dasheds">-</div>
                      <select name="closing_time"  onChange={ onTimeSelectHandlerMultiple(day, food) } className="Selection_box">
                        {timeArray.map( ( time, index ) => (<option key={index} value={ time }>{ time }</option>) )}
                      </select>
                    </div>
                  </td>
                  ) )
                }                
              </tr>
            </tbody>
          </table>
          ))
        }
      </div>
    </>
  )
}

const SeatingArrangement = ({ state, onSeatingAreaArraySelector, onSelectedAreaPrimarySelector, onChange, toggleFunction }) => {
  // console.log( state );
  return (
    <>
          <h2 className='heading-title mt-5'>Do you offer table management?</h2>
            <SlidingComponent
              name='nutri_info'
              // value={this.state.serve_liquor}
              currentState={state.table_management}
              type={'checkbox'}
              spantext1={'Yes'}
              spantext2={'No'}
              toggleclass={'toggle d-flex align-items-center mb-2'}
              toggleinputclass={'toggle__switch ml-3 mr-3'}
              onChange={toggleFunction}
              defaultChecked={false}
            />

          <h2 className='heading-title mt-5'>Where is your Seating Area</h2>
          <div style={{ display:'flex', flexWrap:'wrap' }}>
          { 
            state.all_seating_area_icons.map( ( icon , index ) => (< IconBox key={ index }
              // src={ icon.icon }
              title={ icon.title }
              classsection="main-icon-button"
              onClick={ onSeatingAreaArraySelector( icon ) }
              />) )
          }         
          
          <SuggestNew title='Suggest New' classsection='main-suggest-button' />   
          </div>


          {/** */}
          {
            !isEmpty( state.selected_seating_area ) ? 
            <h2 className='heading-title' style={{ paddingTop:'30px' }}>
              Which is your Primary  Type?
            </h2>
            : null
          }
          
          <div style={{ display:'flex', flexWrap:'wrap' }}>
          { 
            state.selected_seating_area.map( ( icon , index ) => (< IconBox key={ index }
              // src={ icon.icon }
              title={ icon.title }
              classsection={ icon === state.primary_seating_area ? "main-icon-button_active" : "main-icon-button" }
              onClick={ onSelectedAreaPrimarySelector( icon ) }
              />) )
          }


        </div>
        <h2 className='heading-title' style={{ paddingTop:'30px' }}>
              What is your total seating capacity.
            </h2>
            <div><input name='total_seating_capacity' placeholder="eg. 100" style={{ width:'200px' }} className='curve_input_field' value={ state.total_seating_capacity } onChange={ onChange } /></div>
          
          </>
  )
}