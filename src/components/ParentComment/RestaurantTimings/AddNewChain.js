import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import ProgressBar from 'react-bootstrap/ProgressBar';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { create_new_chain } from '../../../store/actions/iconAction';

export class AddNewChain extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
      title: '',
      icons: ''
    };
  }

  /*************************
   * @DESC - MODAL TOGGLER
   *************************/
  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };
  /*************************
   * @DESC - ONCHANGE HANDLER
   *************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /*************************
   * @DESC - ONSUBMIT HANDLER
   *************************/
  onSubmit = e => {
    e.preventDefault();
    let formData = {
      title: this.state.title,
      icon: this.state.icon
    };
    this.props.create_new_chain(formData);
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        this.setState({ icons: res.data.image_URL, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  render() {
    return (
      <>
        <div className='newChain_add' onClick={this.modalToggler}>
          <div>
            <i className='fa fa-plus' style={{ color: '#CCCCCC' }}></i>
          </div>
          <div className='add_new_text'>Add New</div>
        </div>
        <Modal show={this.state.modalShow} onHide={this.modalToggler}>
          <Modal.Body className='modal_body'>
            <div className='modal_head'>New Chain</div>
            <div className='modal_body'>
              <div className='label'>Name of chain</div>
              <input
                type='text'
                name='title'
                onChange={this.onChange}
                placeholder='eg. KFC'
                className='curve_input_field'
              />

              <div className='label'>Restaurant Logo</div>
              {this.state.loader ? (
                <ProgressBar
                  animated
                  variant='danger'
                  label='Uplaoding'
                  now={100}
                />
              ) : null}
              <div className='view_chainsBor' style={{ width: '100%' }}>
                <div className='custom_file_upload'>
                  <input
                    type='file'
                    name='icon'
                    id='file'
                    onChange={this.onImageUploadHandler}
                    className='custom_input_upload'
                  />
                  <label
                    className='custom_input_label newChain_add'
                    htmlFor='file'
                  >
                    <div>
                      <i
                        className='fa fa-plus'
                        style={{ color: '#CCCCCC' }}
                      ></i>
                    </div>
                    <div className='add_new_text'>Add New</div>
                  </label>
                </div>

                <div className='newChain_add mx-3'>
                  {this.state.icons ? (
                    <img
                      src={this.state.icons}
                      className='newChain_add'
                      style={{ height: '100%', width: '100%' }}
                      alt='chain'
                    />
                  ) : null}
                </div>
              </div>

              <div
                className=' mt-4 ml-auto d-flex justify-content-between'
                style={{ width: '70%' }}
              >
                <ButtonComponent
                  buttontext='Back'
                  buttontype='button'
                  buttonclass='btn button-main button-white'
                  onClick={this.modalToggler}
                />
                &nbsp;
                <ButtonComponent
                  buttontext='Submit'
                  buttontype='button'
                  buttonclass='btn button-main button-orange'
                  onClick={this.onSubmit}
                />
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

export default connect(
  null,
  { create_new_chain }
)(withRouter(AddNewChain));
