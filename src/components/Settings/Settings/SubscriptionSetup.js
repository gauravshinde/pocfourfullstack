import React, { Component } from 'react';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';
// import isEmpty from '../../../store/validation/is-Empty';
import CardComponent from '../../../reusableComponents/CardComponent';
import Toggle from '../../../reusableComponents/SlidingComponent';
import axios from 'axios';
import isEmpty from '../../../store/validation/is-Empty';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { get_subscription_details } from './../../../store/actions/addDetailsActions';

const CardContent = [
  {
    icon: ' ',
    title: 'QSR',
    des: ['Curb Side', 'Self Serve', 'Skip the Line', 'Take Away'],
    src: 'https://chainlist.s3.amazonaws.com/dinner+(1)%402x.png'
  }
];

export class SubscriptionSetup extends Component {
  constructor() {
    super();
    this.state = {
      servicesType: '',
      request_period: '',
      placement_period: '',
      foodItem_counter: false,
      takeaway_counter: false,
      uploadMenu: [],
      hasSetDetails: false
    };
  }

  componentDidMount() {
    this.props.get_subscription_details();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    console.log(nextProps.details.subscription_details);
    if (
      !isEmpty(nextProps.details.subscription_details) &&
      !nextState.hasSetDetails
    ) {
      return {
        request_period: nextProps.details.subscription_details.request_period,
        placement_period:
          nextProps.details.subscription_details.placement_period,
        foodItem_counter:
          nextProps.details.subscription_details.foodItem_counter,
        takeaway_counter:
          nextProps.details.subscription_details.takeaway_counter,
        uploadMenu: nextProps.details.subscription_details.uploadMenu,
        hasSetDetails: true
      };
    }
    return null;
  }

  onClickServiceType = data => e => {
    this.setState({
      servicesType: data
    });
  };

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        let uploadMenu = this.state.uploadMenu;
        uploadMenu.push(res.data.image_URL);
        this.setState({ uploadMenu: uploadMenu, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  renderSubscriptionDetails = () => {
    return (
      <React.Fragment>
        <div className='w-50 ml-auto d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            onClick={this.pageChangeHandle(11)}
          />
          <ButtonComponent
            buttontext='Save'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            // onClick={this.props.pageChanger(8)}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    console.log(this.state);
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4>Company Setup</h4>
          <form>
            <div className='row'>
              <div className='col-sm-4'>
                <Cards
                  state={this.state}
                  onClickServiceType={this.onClickServiceType}
                />
              </div>
              <div
                className='col-sm-8'
                style={{ borderLeft: '1px solid #ccc' }}
              >
                <Services
                  state={this.state}
                  onChange={this.onChange}
                  toggleFunction={this.toggleFunction}
                />
                <UploadMenu
                  state={this.state}
                  onImageUploadHandler={this.onImageUploadHandler}
                />
              </div>
            </div>
            {this.renderSubscriptionDetails()}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  details: state.details
});

export default connect(
  mapStateToProps,
  { get_subscription_details }
)(withRouter(SubscriptionSetup));

const Services = ({ state, onChange, toggleFunction }) => {
  return (
    <div className='cuisine-main'>
      <table style={{ width: '100%', borderRadius: '0px' }}>
        <tbody>
          <tr>
            <td>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Request Period
              </h2>
              <div className='d-flex'>
                <input
                  name='request_period'
                  value={state.request_period}
                  onChange={onChange}
                  className='curve_input_field w-50'
                />
                <span
                  className='pt-3 pl-3'
                  style={{
                    fontSize: '15px',
                    color: '#ccc',
                    fontFamily: 'AvenirLTStd-Black'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td className=''>
              <h2
                className='heading-title'
                style={{ paddingTop: '30px', fontSize: '20px' }}
              >
                Placement Period
              </h2>
              <div className='d-flex'>
                <input
                  name='placement_period'
                  value={state.placement_period}
                  onChange={onChange}
                  className='curve_input_field w-50'
                />
                <span
                  className='pt-3 pl-3'
                  style={{
                    fontSize: '15px',
                    color: '#ccc',
                    fontFamily: 'AvenirLTStd-Black'
                  }}
                >
                  Days
                </span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>

      <h2 className='heading-title' style={{ paddingTop: '30px' }}>
        Do you have different counters for different food items?
      </h2>
      <Toggle
        name='foodItem_counter'
        currentState={state.foodItem_counter}
        type={'checkbox'}
        spantext1={'Yes'}
        spantext2={'No'}
        toggleclass={'toggle d-flex align-items-center mb-2'}
        toggleinputclass={'toggle__switch ml-3 mr-3'}
        onChange={toggleFunction}
        defaultChecked={state.foodItem_counter === true ? true : false}
      />

      <h2 className='heading-title' style={{ paddingTop: '30px' }}>
        Do you have a separate counter for takeaway ?
      </h2>
      <Toggle
        name='takeaway_counter'
        currentState={state.takeaway_counter}
        type={'checkbox'}
        spantext1={'Yes'}
        spantext2={'No'}
        toggleclass={'toggle d-flex align-items-center mb-2'}
        toggleinputclass={'toggle__switch ml-3 mr-3'}
        onChange={toggleFunction}
        defaultChecked={state.takeaway_counter === true ? true : false}
      />
    </div>
  );
};

const UploadMenu = ({ state, onImageUploadHandler }) => {
  return (
    <>
      <div className='cuisine-main col-12'>
        <h2 className='heading-title' style={{ paddingTop: '30px' }}>
          Upload Menu
        </h2>
        <div className='view_chainsBor mb-5' style={{ width: '100%' }}>
          <div className='custom_file_upload'>
            <input
              type='file'
              name='icon'
              id='file'
              onChange={onImageUploadHandler}
              className='custom_input_upload'
            />
            <label className='custom_input_label newChain_add' htmlFor='file'>
              <div>
                <i className='fa fa-plus' style={{ color: '#CCCCCC' }}></i>
              </div>
              <div className='add_new_text'>Add New</div>
            </label>
          </div>

          <div className='newChain_addthree mx-3'>
            {!isEmpty(state.uploadMenu) && state.uploadMenu.length > 0
              ? state.uploadMenu.map((image, index) => (
                  <img
                    key={index}
                    src={image}
                    className='newChain_addtwo'
                    style={{ height: '100%', width: '100%' }}
                    alt='chain '
                  />
                ))
              : null}
            {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
          </div>
        </div>
      </div>
    </>
  );
};

const Cards = ({ state, onClickServiceType }) => {
  return (
    <div className='cuisine-main'>
      <h2 className='heading-title' style={{ paddingTop: '30px' }}>
        Your are subscribing to:
      </h2>

      <div className='card_container'>
        <CardComponent
          src='https://chainlist.s3.amazonaws.com/dinner+(1)%402x.png'
          title='QSR'
          des='Curb Side Self Serve Skip the Line Take Away'
          classes={'subscription_card_container_active'}
        />
        {/* {CardContent.map((item, index) => (
          <CardComponent
            key={index}
            item={item}
            src={item.src}
            classes={
              state === item.title
                ? 'main_card_container_active'
                : 'main_card_container'
            }
            onClick={onClickServiceType(item.title)}
          />
        ))} */}
      </div>
    </div>
  );
};
