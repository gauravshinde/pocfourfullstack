import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { AddNewChain } from './../../ParentComment/RestaurantTimings/AddNewChain';
import { ViewAllChains } from './../../ParentComment/RestaurantTimings/ViewAllChains';
import Toggle from './../../../reusableComponents/SlidingComponent';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';
import SlidingComponent from './../../../reusableComponents/SlidingComponent';
import InputComponent from './../../../reusableComponents/InputComponent';
import classnames from 'classnames';

import {
  get_kyc_details,
  get_restauranttime,
  update_kyc_details
} from '../../../store/actions/addDetailsActions';
import isEmpty from './../../../store/validation/is-Empty';

const seating_area = [
  { title: 'Indoor' },
  { title: 'Outdoor' },
  { title: 'Private' },
  { title: 'Bar' }
];

const foodTime = ['breakfast', 'lunch', 'dinner'];

const dayArray = [
  'monday',
  'tuesday',
  'wednesday',
  'thrusday',
  'friday',
  'saturday',
  'sunday'
];

const timeArray = [
  '00:00',
  '01:00',
  '02:00',
  '03:00',
  '04:00',
  '05:00',
  '06:00',
  '07:00',
  '08:00',
  '09:00',
  '10:00',
  '11:00',
  '12:00',
  '13:00',
  '14:00',
  '15:00',
  '16:00',
  '17:00',
  '18:00',
  '19:00',
  '20:00',
  '21:00',
  '22:00',
  '23:00',
  '24:00'
];

export class CompanyDetails extends Component {
  constructor() {
    super();
    this.state = {
      disabled: false,
      all_seating_area_icons: seating_area,
      selected_seating_area: [],
      primary_seating_area: {},
      total_seating_capacity: '',

      part_of_chain: false,
      selectedChains: [],
      are_you_open_24_x_7: false,
      multiple_opening_time: false,
      table_management: false,

      account_name: '',
      bank_name: '',
      account_number: '',
      branch_name: '',
      GST_number: '',
      IFSC_code: '',
      PAN_number: '',
      FSSAI_code: '',
      imgPath: [],
      currentAccount: false,

      errors: {},
      hasSetDetails: false,
      monday: {
        open: false,
        main_opening_time: '',
        main_closing_time: '',
        breakfast: {
          open: false,
          breakfast_opening_time: '',
          breakfast_closing_time: ''
        },
        lunch: {
          open: false,
          lunch_opening_time: '',
          lunch_closing_time: ''
        },
        dinner: {
          open: false,
          dinner_opening_time: '',
          dinner_closing_time: ''
        }
      },
      tuesday: {
        open: false,
        main_opening_time: '',
        main_closing_time: '',
        breakfast: {
          open: false,
          breakfast_opening_time: '',
          breakfast_closing_time: ''
        },
        lunch: {
          open: false,
          lunch_opening_time: '',
          lunch_closing_time: ''
        },
        dinner: {
          open: false,
          dinner_opening_time: '',
          dinner_closing_time: ''
        }
      },
      wednesday: {
        open: false,
        main_opening_time: '',
        main_closing_time: '',
        breakfast: {
          open: false,
          breakfast_opening_time: '',
          breakfast_closing_time: ''
        },
        lunch: {
          open: false,
          lunch_opening_time: '',
          lunch_closing_time: ''
        },
        dinner: {
          open: false,
          dinner_opening_time: '',
          dinner_closing_time: ''
        }
      },
      thrusday: {
        open: false,
        main_opening_time: '',
        main_closing_time: '',
        breakfast: {
          open: false,
          breakfast_opening_time: '',
          breakfast_closing_time: ''
        },
        lunch: {
          open: false,
          lunch_opening_time: '',
          lunch_closing_time: ''
        },
        dinner: {
          open: false,
          dinner_opening_time: '',
          dinner_closing_time: ''
        }
      },
      friday: {
        open: false,
        main_opening_time: '',
        main_closing_time: '',
        breakfast: {
          open: false,
          breakfast_opening_time: '',
          breakfast_closing_time: ''
        },
        lunch: {
          open: false,
          lunch_opening_time: '',
          lunch_closing_time: ''
        },
        dinner: {
          open: false,
          dinner_opening_time: '',
          dinner_closing_time: ''
        }
      },
      saturday: {
        open: false,
        main_opening_time: '',
        main_closing_time: '',
        breakfast: {
          open: false,
          breakfast_opening_time: '',
          breakfast_closing_time: ''
        },
        lunch: {
          open: false,
          lunch_opening_time: '',
          lunch_closing_time: ''
        },
        dinner: {
          open: false,
          dinner_opening_time: '',
          dinner_closing_time: ''
        }
      },
      sunday: {
        open: false,
        main_opening_time: '',
        main_closing_time: '',
        breakfast: {
          open: false,
          opening_time: '',
          closing_time: ''
        },
        lunch: {
          open: false,
          opening_time: '',
          closing_time: ''
        },
        dinner: {
          open: false,
          opening_time: '',
          closing_time: ''
        }
      }
    };
  }

  componentDidMount() {
    this.props.get_kyc_details();
    this.props.get_restauranttime();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    console.log(nextProps.details.restauranttime_details);
    if (
      !isEmpty(nextProps.details.kyc_details) &&
      !isEmpty(nextProps.details.restauranttime_details) &&
      !nextState.hasSetDetails
    ) {
      return {
        total_seating_capacity: '',

        part_of_chain: nextProps.details.restauranttime_details.part_of_chain,
        selectedChains: nextProps.details.restauranttime_details.selectedChains,
        are_you_open_24_x_7:
          nextProps.details.restauranttime_details.are_you_open_24_x_7,
        multiple_opening_time:
          nextProps.details.restauranttime_details.multiple_opening_time,
        table_management:
          nextProps.details.restauranttime_details.table_management,

        monday: {
          open: false,
          main_opening_time: '',
          main_closing_time: '',
          breakfast: {
            open: false,
            breakfast_opening_time: '',
            breakfast_closing_time: ''
          },
          lunch: {
            open: false,
            lunch_opening_time: '',
            lunch_closing_time: ''
          },
          dinner: {
            open: false,
            dinner_opening_time: '',
            dinner_closing_time: ''
          }
        },

        //Company Setup

        account_name: nextProps.details.kyc_details.account_name,
        bank_name: nextProps.details.kyc_details.bank_name,
        account_number: nextProps.details.kyc_details.account_number,
        branch_name: nextProps.details.kyc_details.branch_name,
        GST_number: nextProps.details.kyc_details.GST_number,
        IFSC_code: nextProps.details.kyc_details.IFSC_code,
        PAN_number: nextProps.details.kyc_details.PAN_number,
        FSSAI_code: nextProps.details.kyc_details.FSSAI_code,
        imgPath: nextProps.details.kyc_details.imgPath,
        currentAccount: nextProps.details.kyc_details.currentAccount,
        hasSetDetails: true
      };
    }

    if (!isEmpty(nextProps.details.restauranttime_details) && !nextState)
      return null;
  }

  /***********************
   * @DESC - PAGE CHANGER
   **********************/
  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - TOGGLE FUNCTION
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************
   * @DESC - DISABLE BUTTON
   ***************************/
  onClickCompanySetupDisabled = e => {
    this.setState({
      disabled: true
    });
  };

  /****************************
   * @DESC - DAY TOGGLER -
   ***************************/
  onDayOpenCloseHanlder = day => e => {
    let state = this.state;
    let dayData = state[day];
    dayData.open = !dayData.open;
    this.setState({
      state: state
    });
  };

  onTimeSelectHandlerSingle = day => e => {
    let state = this.state;
    let dayData = state[day];
    dayData[e.target.name] = e.target.value;
    this.setState({
      state: state
    });
  };

  onFoodTypeOpenClose = (day, food) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData.open = !foodTimeData.open;
    this.setState({
      state: state
    });
  };

  onTimeSelectHandlerMultiple = (day, food) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData[e.target.name] = e.target.value;
    this.setState({
      state: state
    });
  };

  /****************************************
   * @DESC - Seating Area CODE SELECTOR
   ****************************************/
  onSeatingAreaArraySelector = icon => e => {
    let SeatingArea = this.state.selected_seating_area;
    if (SeatingArea.length === 0) {
      SeatingArea.push(icon);
    } else {
      let isAlreadyPresent = SeatingArea.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = SeatingArea.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          SeatingArea.splice(indexOf, 1);
        }
      } else {
        SeatingArea.push(icon);
      }
    }
    this.setState({
      selected_seating_area: SeatingArea
    });
  };

  onSelectedAreaPrimarySelector = icon => e => {
    this.setState({
      primary_seating_area: icon
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        let imgPath = this.state.imgPath;
        imgPath.push(res.data.image_URL);
        this.setState({ imgPath: imgPath, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  onSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    const updateData = {
      account_name: this.state.account_name,
      bank_name: this.state.bank_name,
      account_number: this.state.account_number,
      branch_name: this.state.branch_name,
      GST_number: this.state.GST_number,
      IFSC_code: this.state.IFSC_code,
      PAN_number: this.state.PAN_number,
      FSSAI_code: this.state.FSSAI_code,
      imgPath: this.state.imgPath,
      currentAccount: this.state.currentAccount
    };
    this.props.update_kyc_details(updateData);
  };

  renderRestaurantTimings = () => {
    return (
      <React.Fragment>
        {/* <hr className='hr-global mt-5 mb-2' /> */}
        <div className='w-50 ml-auto mt-5 d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            // onClick={this.pageChangeHandle(9)}
          />
          <ButtonComponent
            buttontext='Next'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  renderCompanySetup = () => {
    const { errors } = this.state;
    const { disabled } = this.state;
    return (
      <React.Fragment>
        <h4 className='col-12 mb-4 mt-5 pl-0'>
          Company Setup
          <img
            src={require('../../../assets/images/dashboard/edit.svg')}
            alt='edit'
            className='img-fluid float-right'
            onClick={this.onClickCompanySetupDisabled}
            style={{ width: '2%', height: '2%' }}
          />
        </h4>
        <div className='row'>
          <div className='col-6'>
            <InputComponent
              labeltext='Account Holder Name'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='account_name'
              type='text'
              place='eg. McDonalds'
              onChange={this.onChange}
              value={this.state.account_name}
              error={errors.account_name}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.account_name
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='col-6'>
            <InputComponent
              labeltext='Bank Name'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='bank_name'
              type='text'
              place='eg. McDonalds'
              onChange={this.onChange}
              value={this.state.bank_name}
              error={errors.bank_name}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.bank_name
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='col-6'>
            <InputComponent
              labeltext='Account Number'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='account_number'
              type='text'
              place='eg. McDonalds'
              onChange={this.onChange}
              value={this.state.account_number}
              error={errors.account_number}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.account_number
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='col-6'>
            <InputComponent
              labeltext='Branch Name'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='branch_name'
              type='text'
              place='eg. McDonalds'
              onChange={this.onChange}
              value={this.state.branch_name}
              error={errors.branch_name}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.branch_name
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='col-6'>
            <div className='account-type restaurant-title mt-0'>
              Account Type?
            </div>
            <SlidingComponent
              name='currentAccount'
              currentState={this.state.currentAccount}
              type={'checkbox'}
              spantext1={'Current Account'}
              spantext2={'Savings Account'}
              toggleclass={'toggle d-flex align-items-center mb-2'}
              toggleinputclass={'toggle__switch ml-3 mr-3'}
              onChange={this.toggleFunction}
              defaultChecked={this.state.currentAccount === true ? true : false}
            />
          </div>
          <div className='col-6'>
            <InputComponent
              labeltext='FSSAI Code'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='FSSAI_code'
              type='text'
              place='eg. admin@gmail.com'
              onChange={this.onChange}
              value={this.state.FSSAI_code}
              error={errors.FSSAI_code}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.FSSAI_code
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='col-4'>
            <InputComponent
              labeltext='GST Number'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='GST_number'
              type='text'
              place='eg. Koregaon Park'
              onChange={this.onChange}
              value={this.state.GST_number}
              error={errors.GST_number}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.GST_number
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='col-4'>
            <InputComponent
              labeltext='IFSC Code'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='IFSC_code'
              type='text'
              place='eg. Pune'
              onChange={this.onChange}
              value={this.state.IFSC_code}
              error={errors.IFSC_code}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.IFSC_code
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='col-4'>
            <InputComponent
              labeltext='PAN Number'
              inputlabelclass='input-label'
              imgbox='d-none'
              name='PAN_number'
              type='text'
              place='eg. 9898989898'
              onChange={this.onChange}
              value={this.state.PAN_number}
              error={errors.PAN_number}
              inputclass={
                disabled === false
                  ? 'disable-input-button-css'
                  : classnames('map-inputfield', {
                      invalid: errors.PAN_number
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className='row w-100 mb-3 '>
            <div className='col-sm-4'>
              <p className='restaurant-title pl-3'>Upload Documents</p>
            </div>
            <div className='col-sm-8'>
              <div className='view_chainsBor' style={{ width: '100%' }}>
                <div className='custom_file_upload'>
                  <input
                    type='file'
                    name='icon'
                    id='file'
                    onChange={this.onImageUploadHandler}
                    className='custom_input_upload'
                  />
                  <label
                    className='custom_input_label newChain_add'
                    htmlFor='file'
                  >
                    <div>
                      <i
                        className='fa fa-plus'
                        style={{ color: '#CCCCCC' }}
                      ></i>
                    </div>
                    <div className='add_new_text'>Add New</div>
                  </label>
                </div>

                <div className='newChain_addthree mx-3'>
                  {this.state.imgPath.length > 0
                    ? this.state.imgPath.map((image, index) => (
                        <img
                          key={index}
                          src={image}
                          className='newChain_addtwo'
                          style={{ height: '100%', width: '100%' }}
                          alt='chain '
                        />
                      ))
                    : null}
                  {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
                  {/* <img
                    src={this.state.imgPath}
                    className='newChain_addtwo'
                    style={{ height: '100%', width: '100%' }}
                    alt='upload Kyc'
                  /> */}
                </div>
              </div>
              {/* <div className='multiImageInputFile-outerBlock multiImageInputFile-outerBlock--KYC'>
                <AddNewChain />
                <div className='multiImageInputFile-block'>
                  <img
                    src={require('../../../assets/images/add-file.svg')}
                    alt='add-file-img'
                  />
                  <input
                    className='multiImageInputFile'
                    type='file'
                    multiple
                    title=''
                    onChange={this.handleFilesOnChange}
                  />
                </div>
                <div className='multiImageInputFile-preview'>
                  {this.state.imgPath.map((val, index) => (
                    <div key={index}>
                      <img src={val} alt='list' />
                    </div>
                  ))}
                </div> 
              </div> */}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    console.log(this.state);
    return (
      <React.Fragment>
        <div className='map-setup-yes'>
          <h4 className='col-12 mb-3'>Restaurant Details</h4>
          <form className='pl-3 pr-3'>
            <PartOfChain
              state={this.state}
              toggleFunction={this.toggleFunction}
            />
            <Restauranttiming
              state={this.state}
              toggleFunction={this.toggleFunction}
            />
            {!this.state.multiple_opening_time &&
            !this.state.are_you_open_24_x_7 ? (
              <NoMultipleTime
                state={this.state}
                toggleFunction={this.toggleFunction}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onTimeSelectHandlerSingle={this.onTimeSelectHandlerSingle}
              />
            ) : null}
            {this.state.multiple_opening_time &&
            !this.state.are_you_open_24_x_7 ? (
              <YesMultipleTime
                state={this.state}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onFoodTypeOpenClose={this.onFoodTypeOpenClose}
                onTimeSelectHandlerMultiple={this.onTimeSelectHandlerMultiple}
              />
            ) : null}
            <SeatingArrangement
              state={this.state}
              onSeatingAreaArraySelector={this.onSeatingAreaArraySelector}
              onSelectedAreaPrimarySelector={this.onSelectedAreaPrimarySelector}
              onChange={this.onChange}
            />
            {this.renderCompanySetup()}
            {this.renderRestaurantTimings()}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  details: state.details
});

export default connect(
  mapStateToProps,
  { get_kyc_details, get_restauranttime, update_kyc_details }
)(withRouter(CompanyDetails));

const PartOfChain = ({ state, toggleFunction }) => {
  return (
    <>
      <div className='cuisine-main'>
        <p className='col-12 p-0 restaurant-title'>Are you a part of chain ?</p>
        <Toggle
          name='part_of_chain'
          currentState={state.part_of_chain}
          type={'checkbox'}
          spantext1={'Yes'}
          spantext2={'No'}
          toggleclass={'toggle d-flex align-items-center mb-2'}
          toggleinputclass={'toggle__switch ml-3 mr-3'}
          onChange={toggleFunction}
          defaultChecked={state.part_of_chain === true ? true : false}
        />
      </div>
      {state.part_of_chain ? (
        <div className='cuisine-main mt-4'>
          <table style={{ width: '100%' }}>
            <tbody>
              <tr>
                <td className='seleect_chain'> Select Chain </td>
                <td style={{ width: '10%' }}>
                  <ViewAllChains />
                </td>
              </tr>
            </tbody>
          </table>

          <div className='view_chainsBor'>
            <AddNewChain />
          </div>
        </div>
      ) : null}
    </>
  );
};

const Restauranttiming = ({ state, toggleFunction }) => {
  return (
    <>
      <div className='cuisine-main'>
        <p className='col-12 p-0 restaurant-title'>
          Enter your restaurant timings ?
        </p>
        <p className='col-12 p-0 restaurant-title'>Are you open 24 x 7 ?</p>
        <Toggle
          name='are_you_open_24_x_7'
          currentState={state.are_you_open_24_x_7}
          type={'checkbox'}
          spantext1={'Yes'}
          spantext2={'No'}
          toggleclass={'toggle d-flex align-items-center mb-2'}
          toggleinputclass={'toggle__switch ml-3 mr-3'}
          onChange={toggleFunction}
          defaultChecked={state.are_you_open_24_x_7 === true ? true : false}
        />

        {!state.are_you_open_24_x_7 ? (
          <p className='col-12 p-0 restaurant-title'>
            Do you have multiple opening timings ?
          </p>
        ) : null}
        {!state.are_you_open_24_x_7 ? (
          <Toggle
            name='multiple_opening_time'
            currentState={state.multiple_opening_time}
            type={'checkbox'}
            spantext1={'Yes'}
            spantext2={'No'}
            toggleclass={'toggle d-flex align-items-center mb-2'}
            toggleinputclass={'toggle__switch ml-3 mr-3'}
            onChange={toggleFunction}
            defaultChecked={state.multiple_opening_time === true ? true : false}
          />
        ) : null}
      </div>
    </>
  );
};

const NoMultipleTime = ({
  state,
  toggleFunction,
  onDayOpenCloseHanlder,
  onTimeSelectHandlerSingle
}) => {
  return (
    <>
      <div className='cuisine-main'>
        {dayArray.map((day, index) => (
          <table key={index} style={{ width: '100%' }}>
            <tbody>
              <tr>
                <td>
                  <p className='col-12 p-0 restaurant-title'>{day}</p>
                  <Toggle
                    name={day}
                    currentState={state[day].open}
                    type={'checkbox'}
                    spantext1={'Yes'}
                    spantext2={'No'}
                    toggleclass={'toggle d-flex align-items-center mb-2'}
                    toggleinputclass={'toggle__switch ml-3 mr-3'}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={false}
                  />
                </td>
                <td style={{ width: '30%' }}>
                  {/* OPEN AND CLOSE TIME */}
                  <p className='col-12 p-0 restaurant-title'>Opening Time</p>
                  <div className='opening_time_selector'>
                    <select
                      name='main_opening_time'
                      onChange={onTimeSelectHandlerSingle(day)}
                      className='Selection_box'
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select>
                    <div className='dasheds'>-</div>
                    <select
                      name='main_closing_time'
                      onChange={onTimeSelectHandlerSingle(day)}
                      className='Selection_box'
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const YesMultipleTime = ({
  state,
  onDayOpenCloseHanlder,
  onFoodTypeOpenClose,
  onTimeSelectHandlerMultiple
}) => {
  return (
    <>
      <div className='cuisine-main'>
        {dayArray.map((day, index) => (
          <table key={index} style={{ width: '100%' }}>
            <tbody>
              <tr>
                <td style={{ width: '18%' }}>
                  <p className='col-12 p-0 restaurant-title'>{day}</p>
                  <Toggle
                    name={day}
                    currentState={state[day].open}
                    type={'checkbox'}
                    spantext1={'Yes'}
                    spantext2={'No'}
                    toggleclass={'toggle d-flex align-items-center mb-2'}
                    toggleinputclass={'toggle__switch ml-3 mr-3'}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={false}
                  />
                </td>
                {foodTime.map((food, index) => (
                  <td
                    key={index}
                    style={{
                      width: '200px',
                      paddingLeft: '20px',
                      padding: '15px'
                    }}
                  >
                    {/* OPEN AND CLOSE TIME */}
                    <p className='col-12 p-0 restaurant-title d-flex justify-content-between'>
                      {food}
                      <Toggle
                        name={day}
                        currentState={state[day].open}
                        type={'checkbox'}
                        spantext1={''}
                        spantext2={''}
                        toggleclass={'toggle d-flex align-items-center mb-2'}
                        toggleinputclass={'toggle__switch ml-3 mr-3'}
                        onChange={onFoodTypeOpenClose(day, food)}
                        defaultChecked={false}
                      />
                    </p>
                    <div className='opening_time_selector'>
                      <select
                        name='opening_time'
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className='Selection_box'
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select>
                      <div className='dasheds'>-</div>
                      <select
                        name='closing_time'
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className='Selection_box'
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select>
                    </div>
                  </td>
                ))}
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const SeatingArrangement = ({ state, onChange }) => {
  console.log(state);
  return (
    <React.Fragment>
      <p className='col-12 p-0 restaurant-title'>
        Do you Offer Table Management
      </p>
      <SlidingComponent
        //   name={day}
        currentState={state.table_management}
        type={'checkbox'}
        spantext1={'Yes'}
        spantext2={'No'}
        toggleclass={'toggle d-flex align-items-center mb-2'}
        toggleinputclass={'toggle__switch ml-3 mr-3'}
        //   onChange={onDayOpenCloseHanlder(day)}
        defaultChecked={state.table_management === true ? true : false}
      />
    </React.Fragment>
  );
};
