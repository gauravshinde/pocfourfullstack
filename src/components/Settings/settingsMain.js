import React, { Component } from 'react';
import NavBar from '../Dassboard/NavbarDashboard';
import Modal from 'react-bootstrap/Modal';
import StoreSetting from './Settings/StoreSetting';
import RestaurantsDetails from './Settings/RestaurantSetting';
import CuisineSetup from './Settings/CuisineSetup';
import CompanyDetails from './Settings/CompanyDetails';
import SubscriptonDetails from './Settings/SubscriptionSetup';
import axios from 'axios';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  get_restaurants_type_details,
  get_restaurant_details
} from './../../store/actions/addDetailsActions';
import isEmpty from './../../store/validation/is-Empty';
import ButtonComponent from '../../reusableComponents/ButtonComponent';

export class settingsMain extends Component {
  constructor() {
    super();
    this.state = {
      selectedPAge: 1,
      restaurant_name: '',
      restaurant_address: '',
      restaurant_logo: '',
      modalShow: false
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectedPAge: value
    });
  };

  componentDidMount() {
    this.props.get_restaurant_details();
    this.props.get_restaurants_type_details();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      !isEmpty(nextProps.details.mapDetails) &&
      !isEmpty(nextProps.details.restaurant_details)
    ) {
      return {
        restaurant_name: nextProps.details.mapDetails.restaurant_name,
        restaurant_address: nextProps.details.mapDetails.restaurant_address,
        restaurant_logo: nextProps.details.restaurant_details.restaurant_logo
      };
    }
  }

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageLogoUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append('image', e.target.files[0]);
    axios
      .post('/image/upload-content-images', data)
      .then(res => {
        this.setState({ restaurant_logo: res.data.image_URL, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert('Error while uploading the image');
      });
  };

  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  render() {
    console.log(this.state);
    return (
      <>
        <div className='container-fluid'>
          <div className='row'>
            <NavBar />
          </div>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='main_my_settings_page'>My Settings</div>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-12 mb-3'>
              <div className='main_settings_container'>
                <div className='left_settings_panel'>
                  <div className='imagge_container'>
                    <img
                      src={this.state.restaurant_logo}
                      alt='restaurent'
                      className='img-fluid restaurant-image'
                      onClick={this.modalToggler}
                    />
                  </div>
                  <Modal
                    show={this.state.modalShow}
                    size='md'
                    onHide={this.modalToggler}
                    centered
                  >
                    <Modal.Body className='p-0'>
                      <div className='suggest-new-title'>
                        <h3>Change Logo</h3>
                      </div>
                      <div className='main-change-logo'>
                        <h5>Restaurant Logo</h5>
                        <div
                          className='view_chainsBor'
                          style={{ width: '100%' }}
                        >
                          <div className='custom_file_upload'>
                            <input
                              type='file'
                              name='icon'
                              id='file'
                              onChange={this.onImageLogoUploadHandler}
                              className='custom_input_upload'
                            />
                            <label
                              className='custom_input_label newChain_add'
                              htmlFor='file'
                            >
                              <div>
                                <i
                                  className='fa fa-plus'
                                  style={{ color: '#CCCCCC' }}
                                ></i>
                              </div>
                              <div className='add_new_text'>Add New</div>
                            </label>
                          </div>

                          <div className='newChain_addthree mx-3'>
                            {this.state.restaurant_logo ? (
                              <img
                                src={this.state.restaurant_logo}
                                className='newChain_addtwo'
                                style={{ height: '100%', width: '100%' }}
                                alt='chain'
                              />
                            ) : null}
                          </div>
                        </div>
                      </div>
                      <div className='col-10 ml-auto mt-2 mb-4 d-flex justify-content-between'>
                        <ButtonComponent
                          buttontext='Cancel'
                          buttontype='button'
                          buttonclass='btn button-main button-white'
                          onClick={this.modalToggler}
                        />
                        <ButtonComponent
                          buttontext='Send'
                          buttontype='button'
                          buttonclass='btn button-main button-orange'
                        />
                      </div>
                    </Modal.Body>
                  </Modal>
                  <div className='name_container'>
                    <h4>{this.state.restaurant_name}</h4>{' '}
                    {/* <img
                      src={require('../../assets/images/dashboard/edit.svg')}
                      alt='edit'
                      className='img-fluid edit-restaurant-image'
                    /> */}
                  </div>
                  <div className='address_container'>
                    <p>{this.state.restaurant_address}</p>
                    <hr />
                  </div>
                  <div className='menu_container'>
                    <table style={{ borderRadius: '0px' }}>
                      <tbody>
                        <tr
                          onClick={this.onPageChange(1)}
                          className={
                            this.state.selectedPAge === 1
                              ? 'menu_container_active'
                              : 'menu_container'
                          }
                        >
                          <td>
                            <div className='circle_settng'></div>
                          </td>
                          <td className='circle_label'> Store Setting </td>
                        </tr>
                        <tr
                          onClick={this.onPageChange(2)}
                          className={
                            this.state.selectedPAge === 2
                              ? 'menu_container_active'
                              : 'menu_container'
                          }
                        >
                          <td>
                            <div className='circle_settng'></div>
                          </td>
                          <td className='circle_label'> Restaurant Setting </td>
                        </tr>
                        <tr
                          onClick={this.onPageChange(3)}
                          className={
                            this.state.selectedPAge === 3
                              ? 'menu_container_active'
                              : 'menu_container'
                          }
                        >
                          <td>
                            <div className='circle_settng'></div>
                          </td>
                          <td className='circle_label'> Cuisine Setup </td>
                        </tr>
                        <tr
                          onClick={this.onPageChange(4)}
                          className={
                            this.state.selectedPAge === 4
                              ? 'menu_container_active'
                              : 'menu_container'
                          }
                        >
                          <td>
                            <div className='circle_settng'></div>
                          </td>
                          <td className='circle_label'> Company Details </td>
                        </tr>
                        <tr
                          onClick={this.onPageChange(5)}
                          className={
                            this.state.selectedPAge === 5
                              ? 'menu_container_active'
                              : 'menu_container'
                          }
                        >
                          <td>
                            <div className='circle_settng'></div>
                          </td>
                          <td className='circle_label'>
                            {' '}
                            Subscription Setting{' '}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className='right_settings_panel'>
                  <div id='style-4' className='inside-right-pannel'>
                    {this.state.selectedPAge === 1 ? <StoreSetting /> : ''}
                    {this.state.selectedPAge === 2 ? (
                      <RestaurantsDetails />
                    ) : (
                      ''
                    )}
                    {this.state.selectedPAge === 3 ? <CuisineSetup /> : ''}
                    {this.state.selectedPAge === 4 ? <CompanyDetails /> : ''}
                    {this.state.selectedPAge === 5 ? (
                      <SubscriptonDetails />
                    ) : (
                      ''
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  details: state.details
});

export default connect(
  mapStateToProps,
  { get_restaurants_type_details, get_restaurant_details }
)(withRouter(settingsMain));
