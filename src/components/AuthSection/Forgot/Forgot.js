import React, { Component } from 'react';
import InputComponent from '../../../reusableComponents/InputComponent';
import HeaderComponent from '../../../reusableComponents/HeaderComponent';
import isEmpty from './../../../store/validation/is-Empty';
import OtpInputSection from './../Otp/OtpInputSection';
import classnames from 'classnames';
import ButtonComponent from './../../../reusableComponents/ButtonComponent';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  forgot_sendotp,
  forgot_new_password
} from '../../../store/actions/authActions';

export class Forgot extends Component {
  constructor() {
    super();
    this.state = {
      mobile_number: '',
      render: false,
      otpNumOne: '',
      otpNumTwo: '',
      otpNumThree: '',
      otpNumFour: '',
      otpNumFive: '',
      otpNumSix: '',
      password: '',
      confirm_password: '',
      errors: {}
    };
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /******************************
   *  @DESC - ON NUMBER DATA SEND
   *******************************/
  onOtpSend = e => {
    e.preventDefault();
    if (!isEmpty(this.state.mobile_number)) {
      this.setState({
        render: true
      });
    } else {
      this.setState({
        render: false
      });
    }
    let formData = {
      mobile_number: this.state.mobile_number
    };
    this.props.forgot_sendotp(formData);
  };

  /**************************
   *  @DESC - ON DATA SUBMIT
   ***************************/
  handleSendSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    const { password, confirm_password } = this.state;

    let formData = {
      mobile_number: this.state.mobile_number,
      password: this.state.password,
      confirm_password: this.state.confirm_password,
      OTP:
        this.state.otpNumOne +
        this.state.otpNumTwo +
        this.state.otpNumThree +
        this.state.otpNumFour +
        this.state.otpNumFive +
        this.state.otpNumSix
    };
    console.log(formData);
    if (password !== confirm_password) {
      alert("Passwords don't match");
    } else {
      this.props.forgot_new_password(formData);
      window.alert('Your Password Reset Successfully');
      window.location.href = '/login';
    }
  };

  renderForgotSection = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <InputComponent
          labeltext='phone number'
          inputlabelclass='input-label'
          imgbox='img-box'
          imgsrc={require('../../../assets/images/icons/phone.svg')}
          imgclass='img-fluid img pl-1'
          name='mobile_number'
          type='text'
          place='eg. 1234567890'
          onChange={this.onChange}
          value={this.state.mobile_number}
          error={errors.mobile_number}
          inputclass={classnames('input-field', {
            invalid: errors.mobile_number
          })}
        />
        <div className='row'>
          <div className='col-12 text-right'>
            <ButtonComponent
              buttontext='Send'
              buttontype='submit'
              buttonclass='btn button-main button-orange'
              onClick={this.onOtpSend}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  renderForgotPasswordSection = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        {/* <h3 className='text-center'>OTP</h3> */}
        <div className='otp-inputfield-class'>
          <div className='input-all-field'>
            <OtpInputSection
              name='otpNumOne'
              onChange={this.onChange}
              value={this.state.otpNumOne}
              error={errors.otpNumOne}
              inputclass={classnames('input-field', {
                invalid: errors.otpNumOne
              })}
            />
            <OtpInputSection
              name='otpNumTwo'
              onChange={this.onChange}
              value={this.state.otpNumTwo}
              error={errors.otpNumTwo}
              inputclass={classnames('input-field', {
                invalid: errors.otpNumTwo
              })}
            />
            <OtpInputSection
              name='otpNumThree'
              onChange={this.onChange}
              value={this.state.otpNumThree}
              error={errors.otpNumThree}
              inputclass={classnames('input-field', {
                invalid: errors.otpNumThree
              })}
            />
            <OtpInputSection
              name='otpNumFour'
              onChange={this.onChange}
              value={this.state.otpNumFour}
              error={errors.otpNumFour}
              inputclass={classnames('input-field', {
                invalid: errors.otpNumFour
              })}
            />
            <OtpInputSection
              name='otpNumFive'
              onChange={this.onChange}
              value={this.state.otpNumFive}
              error={errors.otpNumFive}
              inputclass={classnames('input-field', {
                invalid: errors.otpNumFive
              })}
            />
            <OtpInputSection
              name='otpNumSix'
              onChange={this.onChange}
              value={this.state.otpNumSix}
              error={errors.otpNumSix}
              inputclass={classnames('input-field', {
                invalid: errors.otpNumSix
              })}
            />
          </div>
        </div>
        <InputComponent
          labeltext='Password'
          inputlabelclass='input-label pt-3'
          imgbox='img-box'
          imgsrc={require('../../../assets/images/icons/lock.svg')}
          imgclass='img-fluid img pl-1'
          name='password'
          type='password'
          place='******'
          onChange={this.onChange}
          value={this.state.password}
          inputclass='input-field'
        />
        <InputComponent
          labeltext='Confirm Password'
          inputlabelclass='input-label'
          imgbox='img-box'
          imgsrc={require('../../../assets/images/icons/lock.svg')}
          imgclass='img-fluid img pl-1'
          name='confirm_password'
          type='password'
          place='******'
          onChange={this.onChange}
          value={this.state.confirm_password}
          inputclass='input-field'
        />
        <div className='row'>
          <div className='col-12 text-right'>
            <ButtonComponent
              buttontext='Send'
              buttontype='submit'
              buttonclass='btn button-main button-orange'
              onClick={this.handleSendSubmit}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    console.log(this.state.render);
    const { render } = this.state;
    return (
      <React.Fragment>
        <HeaderComponent />
        <div className='container-fluid login-background-banner main-Login'>
          <div className='row'>
            <div className='login-background'>
              <div className='login_orange'>
                <h1>Forgot Password</h1>
              </div>
              <div className='login_padding'>
                {render ? (
                  <form>{this.renderForgotPasswordSection()}</form>
                ) : (
                  <form onSubmit={this.handleSendSubmit}>
                    {this.renderForgotSection()}
                  </form>
                )}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(
  null,
  { forgot_sendotp, forgot_new_password }
)(withRouter(Forgot));
