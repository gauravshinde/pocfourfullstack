import React, { Component } from 'react';

class OtpInputSection extends Component {
  render() {
    const { name, value, onChange } = this.props;
    return (
      <>
        <input
          className='otp-input-box'
          type='text'
          pattern='[0-9]*'
          name={name}
          placeholder=''
          value={value}
          onChange={onChange}
          maxLength='1'
        />

        <span className='otp-input-dash'>&#45;</span>
      </>
    );
  }
}

export default OtpInputSection;
