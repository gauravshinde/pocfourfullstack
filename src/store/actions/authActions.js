import axios from 'axios';
import { SET_LOADER, CLEAR_LOADER, SET_CURRENT_USER } from '../types';
import { setErrors } from './errorActions';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

export const create_new_user = (formData, history) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let user_sign_up = await axios.post('/users/signup', formData);
    if (user_sign_up.data) {
      dispatch({ type: CLEAR_LOADER });
      history.push({
        pathname: '/otp',
        state: { user_mobile_number: formData.mobile_number }
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const forgot_sendotp = (formData, callback) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let forgot_sendotp = await axios.post('/users/sendOtp', formData);
    if (forgot_sendotp.data) {
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const forgot_new_password = (formData, callback) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let forgot_new_password = await axios.patch(
      '/users/forgot-password',
      formData
    );
    if (forgot_new_password.data) {
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const verify_otp = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let verifyOtp = await axios.post('/users/verifyOtp', formData);
    if (verifyOtp.data) {
      dispatch({ type: CLEAR_LOADER });
      window.alert('OTP Successfully Verified');
      window.location.href = '/';
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_mobile_number = (formData, callback) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let update_mobile_number = await axios.patch(
      '/users/update_mobile_number',
      formData
    );
    if (update_mobile_number.data) {
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const resendOtp = (formData, callback) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let resendOTP = await axios.patch('/users/sendOtp', formData);
    if (resendOTP.data) {
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const user_login = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let user_login = await axios.post('/users/login', formData);
    if (user_login.data.success) {
      dispatch({ type: CLEAR_LOADER });
      localStorage.clear();
      const { token } = user_login.data;
      // SET Data to Local Storage
      localStorage.setItem('jwtToken', token);
      // Set token To Auth Headers
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      dispatch(set_current_user(decoded));
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/*****************************
 * @DESC - SET CURRRENT USER
 ****************************/
export const set_current_user = decoded => async dispatch => {
  dispatch({
    type: SET_CURRENT_USER,
    payload: decoded
  });
};

/******************************
 * @DESC - LOGOUT USER
 ****************************/
export const logOutUser = () => async dispatch => {
  // REMOVE THE TOKEN
  await localStorage.clear();
  await setAuthToken(false);
  dispatch(set_current_user({}));
  window.location.href = '/';
};
