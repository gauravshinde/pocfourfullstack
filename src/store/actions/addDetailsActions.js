import axios from 'axios';
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_MAP_DETAILS,
  GET_POC_LIST,
  GET_RESTAURANT_DETAILS,
  GET_SERVICE_DETAILS,
  GET_RESTAURANT_FEATURES,
  GET_CUSINE_ONE,
  GET_CUSINE_TWO,
  GET_RESTAURANTTIME_DETAILS,
  GET_KYC_DETAILS,
  GET_SUBSCRIPTION_DETAILS,
  SET_STATUS
} from '../types';
import { setErrors } from './errorActions';

export const create_restaraunttime = (formData, callback) => async dispatch => {
  console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post('/details/add-restaurantTime', formData);
    if (new_poc.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_Kycdetails = (formData, callback) => async dispatch => {
  console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post('/details/add-kycbank-details', formData);
    if (new_poc.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

// export const create_new_subscription = formData => dispatch => {
//   console.log(formData);
//   axios
//     .post('/details/add-subscription-details')
//     // .then(res => console.log(res.data))
//     .then(res => {
//       if (res.status === 200) {
//         dispatch({
//           type: SET_STATUS,
//           payload: '200'
//         });
//       }

//       console.log(res.status);
//     })
//     .catch(err => console.log(err));
// };

export const create_new_subscription = (
  formData,
  callback
) => async dispatch => {
  console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post(
      '/details/add-subscription-details',
      formData
    );
    if (new_poc.data) {
      console.log(new_poc.status);

      dispatch({ type: SET_STATUS, payload: new_poc.status });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_new_poc = (formData, callback) => async dispatch => {
  console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post('/details/add-poc', formData);
    if (new_poc.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_new_restaurant = (formData, callback) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post('/details/add-restaurant', formData);
    if (new_restaurant.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET RESTAURANT DETAILS
 *****************************************/

export const get_restaurant_details = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get('/details/get-restaurant');
    if (get_restaurant.data) {
      console.log(get_restaurant.data);
      dispatch({
        type: GET_MAP_DETAILS,
        payload: get_restaurant.data
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE RESTAURANT DETAILS
 *****************************************/

export const update_restaurant_details = formData => dispatch => {
  console.log(formData);
  axios
    .patch('/details/update-restaurant-details/', formData)
    // .patch("/all_vendor/:vendorId", editUser)
    .then(res => console.log(res.data))
    .catch(err => console.log(err));
};

/***************************************
 * @DESC - POC DETAILS SECTION
 *****************************************/

export const get_poc_details = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get('/details/get-poc');
    if (get_restaurant.data) {
      console.log(get_restaurant.data);
      dispatch({
        type: GET_POC_LIST,
        payload: get_restaurant.data
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const delete_poc_contact_details = deleteData => dispatch => {
  axios
    .delete('/details/delete-poc', deleteData)
    .then(res => console.log(res.data))
    .catch(err => console.log(err));
};

export const create_new_service_details = (
  formData,
  callback
) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      '/details/add-service-details',
      formData
    );
    if (new_restaurant.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_new_resto_features = (
  formData,
  callback
) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      '/details/add-restoFeatures',
      formData
    );
    if (new_restaurant.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_cusine_one = (formData, callback) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post('/details/add-cusineOne', formData);
    if (new_restaurant.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_cusine_two = (formData, callback) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post('/details/add-cusineTwo', formData);
    if (new_restaurant.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_restaurants_type_details = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get('/details/get-restoType');
    if (get_restaurant.data) {
      dispatch({
        type: GET_RESTAURANT_DETAILS,
        payload: get_restaurant.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_services_details = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get('/details/get-service-details');
    if (get_restaurant.data) {
      dispatch({
        type: GET_SERVICE_DETAILS,
        payload: get_restaurant.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_restaurant_features = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get('/details/get-restoFeatures');
    if (get_restaurant.data) {
      dispatch({
        type: GET_RESTAURANT_FEATURES,
        payload: get_restaurant.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_cusine_one = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get('/details/get-cusineOne');
    if (get_restaurant.data) {
      dispatch({
        type: GET_CUSINE_ONE,
        payload: get_restaurant.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_cusine_two = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get('/details/get-cusineTwo');
    if (get_restaurant.data) {
      dispatch({
        type: GET_CUSINE_TWO,
        payload: get_restaurant.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET RESTAURANTTIME DETAILS
 *****************************************/
export const get_restauranttime = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restauranttime = await axios.get('/details/get-restaurantTime');
    if (get_restauranttime.data) {
      dispatch({
        type: GET_RESTAURANTTIME_DETAILS,
        payload: get_restauranttime.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET KYC DETAILS
 *****************************************/
export const get_kyc_details = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_kyc_details = await axios.get('/details/get-kycbank-details');
    if (get_kyc_details.data) {
      dispatch({
        type: GET_KYC_DETAILS,
        payload: get_kyc_details.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE KYC DETAILS
 *****************************************/
export const update_kyc_details = updateData => dispatch => {
  axios.patch('/details/update-kycbank-details', updateData);
  // .then(res => console.log(res.data));
  // .catch(err => console.log(err));
  // dispatch({ type: SET_LOADER });
  // try {
  //   let update_kyc_details = await axios.get('/details/get-kycbank-details');
  //   if (get_kyc_details.data) {
  //     dispatch({
  //       type: GET_KYC_DETAILS,
  //       payload: get_kyc_details.data
  //     });
  //   }
  // } catch (err) {
  //   dispatch({ type: CLEAR_LOADER });
  //   dispatch(setErrors(err));
  // }
};

/***************************************
 * @DESC - GET SUBSCRIPTION DETAILS
 *****************************************/
export const get_subscription_details = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_subscription_detailsone = await axios.get(
      '/details/get-subscription-details'
    );
    if (get_subscription_detailsone.data) {
      dispatch({
        type: GET_SUBSCRIPTION_DETAILS,
        payload: get_subscription_detailsone.data
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

// get.('icons/get-chain')

/***************************************
 * @DESC - UPDATE RESTAURANT SETTING DETAILS
 *****************************************/
export const update_restaurant_setting = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_setting = await axios.patch(
      '/details/update-restaurant',
      formData
    );
    console.log(update_restaurant_setting.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE RESTAURANT SETTING DETAILS
 *****************************************/
export const update_restaurant_features = newData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_features = await axios.patch(
      '/details/update-restoFeatures',
      newData
    );
    console.log(update_restaurant_features.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE CUISINE ONE DETAILS
 *****************************************/
export const update_cuisine_one = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let update_cuisine_one = await axios.patch(
      '/details/update-cusineOne',
      formData
    );
    console.log(update_cuisine_one.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE CUISINE TWO DETAILS
 *****************************************/
export const update_cuisine_two = newData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let update_cuisine_two = await axios.patch(
      '/details/update-cusineTwo',
      newData
    );
    console.log(update_cuisine_two.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - ALL SUGGEST NEW BUTTON ACTION
 *****************************************/
export const create_new_suggest_category = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_suggestbutton = await axios.post('/details/suggest-new', formData);
    if (new_suggestbutton.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      window.alert('Your Suggestion Submited Successfully');
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - ALL SUGGEST NEW BUTTON ACTION
 *****************************************/
export const create_new_addmore_category = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_suggestbutton = await axios.post('/details/suggest-new', formData);
    if (new_suggestbutton.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      window.alert('Add More Submited Successfully');
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - RESTAURANT DETAILS
 *****************************************/
export const create_new_restraunt_type = (
  formData,
  callback
) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post('/details/add-restoType', formData);
    if (new_restaurant.data) {
      console.log('adasd');
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

// export const create_new_dress_code = (formData, callback) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let dress_code = await axios.post('', formData);
//     if (dress_code.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// export const create_new_payment_methods = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let payment_methods = await axios.post('', formData);
//     if (payment_methods.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// /***************************************
//  * @DESC - SERVICE DETAILS
//  *****************************************/
// export const create_new_facility_premises = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let facility_premises = await axios.post('', formData);
//     if (facility_premises.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// export const create_new_service_provide = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let service_provide = await axios.post('', formData);
//     if (service_provide.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// /***************************************
//  * @DESC - RESTAURENT FEATURES
//  *****************************************/
// export const create_new_features_provide = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let features_provide = await axios.post('', formData);
//     if (features_provide.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// export const create_new_accessible_restaurent = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let accessible_restaurent = await axios.post('', formData);
//     if (accessible_restaurent.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// export const create_new_parking_provide = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let parking_provide = await axios.post('', formData);
//     if (parking_provide.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// /***************************************
//  * @DESC - CUISINE FEATURES
//  *****************************************/
// export const create_new_food_serve = (formData, callback) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let food_serve = await axios.post('', formData);
//     if (food_serve.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// export const create_new_cuisine_serve = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let cuisine_serve = await axios.post('', formData);
//     if (cuisine_serve.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// /***************************************
//  * @DESC - RESTAURENT TIMINGS
//  *****************************************/
// export const create_new_restaurenttiming_seating_area = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let restaurenttiming_seating_area = await axios.post('', formData);
//     if (restaurenttiming_seating_area.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// /***************************************
//  * @DESC - SUBSCRIPTION DETAILS CASUAL DINING
//  *****************************************/
// export const create_new_subscription_seating_area = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let subscription_seating_area = await axios.post('', formData);
//     if (subscription_seating_area.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };

// /***************************************
//  * @DESC - SUBSCRIPTION DETAILS FSR
//  *****************************************/
// export const create_new_subscription_fsr_seating_area = (
//   formData,
//   callback
// ) => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let subscription_fsr_seating_area = await axios.post('', formData);
//     if (subscription_fsr_seating_area.data) {
//       console.log('adasd');
//       dispatch({ type: CLEAR_LOADER });
//       callback();
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };
