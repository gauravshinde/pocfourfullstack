import {
  GET_MAP_DETAILS,
  GET_POC_LIST,
  GET_RESTAURANT_DETAILS,
  GET_RESTAURANT_FEATURES,
  GET_SERVICE_DETAILS,
  GET_CUSINE_ONE,
  GET_CUSINE_TWO,
  GET_RESTAURANTTIME_DETAILS,
  GET_KYC_DETAILS,
  GET_SUBSCRIPTION_DETAILS
} from '../types';

const initialState = {
  mapDetails: {},
  poc_list: [],
  restaurant_details: [],
  service_details: [],
  restaurant_features: [],
  cuisine_one: [],
  cuisine_two: [],
  restauranttime_details: [],
  kyc_details: {},
  subscription_details: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_MAP_DETAILS:
      console.log(action.payload);
      return {
        ...state,
        mapDetails: action.payload
      };
    case GET_POC_LIST:
      return {
        ...state,
        poc_list: action.payload
      };
    case GET_RESTAURANT_DETAILS:
      return {
        ...state,
        restaurant_details: action.payload
      };
    case GET_RESTAURANT_FEATURES:
      return {
        ...state,
        restaurant_features: action.payload
      };
    case GET_SERVICE_DETAILS:
      return {
        ...state,
        service_details: action.payload
      };
    case GET_CUSINE_ONE:
      return {
        ...state,
        cuisine_one: action.payload
      };
    case GET_RESTAURANTTIME_DETAILS:
      console.log(action.payload);
      return {
        ...state,
        restauranttime_details: action.payload
      };
    case GET_CUSINE_TWO:
      return {
        ...state,
        cuisine_two: action.payload
      };
    case GET_KYC_DETAILS:
      return {
        ...state,
        kyc_details: action.payload
      };
    case GET_SUBSCRIPTION_DETAILS:
      return {
        ...state,
        subscription_details: action.payload
      };
    default:
      return state;
  }
}
