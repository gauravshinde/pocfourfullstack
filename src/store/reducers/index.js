import { combineReducers } from 'redux';
import errorReducer from './errorReducers';
import authReducers from './authReducers';
import iconReducers from './iconReducers';
import restaurantReducers from './restaurantReducers';

export default combineReducers({
  errors: errorReducer,
  auth: authReducers,
  icons: iconReducers,
  details: restaurantReducers
});
