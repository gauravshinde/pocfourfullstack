// import isEmpty from '../validation/is-Empty';
// import validator from 'validator';

// export const KycDetailsValidation = data => {
//   let errors = {};

//   data.account_name = !isEmpty(data.account_name) ? data.account_name : '';
//   data.bank_name = !isEmpty(data.bank_name) ? data.bank_name : '';
//   data.account_number = !isEmpty(data.account_number)
//     ? data.account_number
//     : '';
//   //   data.branch_name = !isEmpty(data.branch_name) ? data.branch_name : '';
//   //   data.GST_number = !isEmpty(data.GST_number) ? data.GST_number : '';
//   //   data.IFSC_code = !isEmpty(data.IFSC_code) ? data.IFSC_code : '';
//   //   data.PAN_number = !isEmpty(data.PAN_number) ? data.PAN_number : '';
//   //   data.FSSAI_code = !isEmpty(data.FSSAI_code) ? data.FSSAI_code : '';

//   // Account Name
//   if (!validator.isLength(data.account_name, { min: 2, max: 50 })) {
//     errors.account_name = 'Account Name Minimum 2 Char and Maximum 50 Char';
//   }
//   if (validator.isEmpty(data.account_name)) {
//     errors.account_name = 'Account Name Is Required *';
//   }

//   // Bank Name
//   if (!validator.isLength(data.bank_name, { min: 2, max: 20 })) {
//     errors.bank_name = 'Bank Name Minimum 2 Char and Maximum 20 Char';
//   }
//   if (validator.isEmpty(data.bank_name)) {
//     errors.bank_name = 'Bank Name Is Required *';
//   }

//   // Account Number
//   if (!validator.isLength(data.account_number, { min: 0, max: 20 })) {
//     errors.account_number = 'Account Number Maximum 20 Char';
//   }
//   if (validator.isEmpty(data.account_number)) {
//     errors.account_number = 'Account Number Is Required *';
//   }

//   return {
//     errors,
//     isValid: isEmpty(errors)
//   };
// };
