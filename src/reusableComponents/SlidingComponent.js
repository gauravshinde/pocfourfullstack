import React from 'react';

const SlidingComponent = ({
  name,
  toggleclass,
  type,
  toggleinputclass,
  spantext1,
  spantext2,
  currentState,
  onChange,
  defaultChecked
}) => {
  return (
    <React.Fragment>
      <div className={toggleclass}>
        <span>{currentState ? <b>{spantext1}</b> : spantext1}</span>
        <input
          type={type}
          name={name}
          className={`${toggleinputclass}`}
          onChange={onChange}
          defaultChecked={defaultChecked}
        />
        <span>{currentState ? spantext2 : <b>{spantext2}</b>}</span>
      </div>
    </React.Fragment>
  );
};

export default SlidingComponent;
