import React from 'react';

const InputComponent = ({
  inputlabelclass,
  labeltext,
  imgbox,
  imgsrc,
  imgclass,
  name,
  value,
  type,
  place,
  onChange,
  inputclass,
  error,
  disabled
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <div className='input-container'>
        <div className={imgbox}>
          <img src={imgsrc} alt='icon' className={imgclass} />
        </div>
        <input
          name={name}
          value={value}
          type={type}
          className={inputclass}
          placeholder={place}
          onChange={onChange}
          disabled={disabled === true ? { disabled } : false}
        />
      </div>
      {error ? <div className='error'>{error}</div> : null}
    </React.Fragment>
  );
};

export default InputComponent;
