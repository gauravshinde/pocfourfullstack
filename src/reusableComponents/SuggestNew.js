import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import InputComponent from './InputComponent';
import classnames from 'classnames';
import TextAreaComponent from './TextAreaComponent';
import ButtonComponent from './ButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  create_new_restraunt_type,
  create_new_suggest_category
} from './../store/actions/addDetailsActions';

export class SuggestNew extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
      suggest_title: '',
      suggest_category: '',
      suggest_comment: '',
      errors: {}
    };
  }

  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - ONSUBMIT HANDLER
   ***************************/
  onSuggestSubmit = e => {
    e.preventDefault();
    let formData = {
      suggest_title: this.state.suggest_title,
      suggest_category: this.state.suggest_category,
      suggest_comment: this.state.suggest_comment
    };
    this.props.create_new_suggest_category(formData, this.modalToggler);
  };

  rendreSuggestButtonModal = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <div className='inside-body-section'>
          <InputComponent
            labeltext='Title'
            inputlabelclass='input-label-suggest'
            imgbox='d-none'
            name='suggest_title'
            type='text'
            place='eg. Jhatka'
            onChange={this.onChange}
            value={this.state.suggest_title}
            error={errors.suggest_title}
            inputclass={classnames('map-inputfield', {
              invalid: errors.suggest_title
            })}
          />
          <div className='form-group'>
            <label htmlFor='category' className='input-label-suggest'>
              Category
            </label>
            <select
              name='suggest_category'
              onChange={this.onChange}
              error={errors.suggest_category}
              className={classnames('select-category ', {
                invalid: errors.suggest_category
              })}
            >
              <option>Select Category</option>
              <option value='Restaurant Details - Type'>
                Restaurant Details - Type
              </option>
              <option value='Restaurant Details - Dress Code'>
                Restaurant Details - Dress Code
              </option>
              <option value='Restaurant Details - Payments Methods'>
                Restaurant Details - Payments Methods
              </option>
              <option value='Services Details - Facility'>
                Services Details - Facility
              </option>
              <option value='Services Details - Services'>
                Services Details - Services
              </option>
              <option value='Restaurant Features - Features'>
                Restaurant Features - Features
              </option>
              <option value='Restaurant Features - Accessible'>
                Restaurant Features - Accessible
              </option>
              <option value='Restaurant Features - Parking'>
                Restaurant Features - Parking
              </option>
              <option value='Cuisine Features - Food Category'>
                Cuisine Features - Food Category
              </option>
              <option value='Cuisine Features - Food Items'>
                Cuisine Features - Food Items
              </option>
              <option value='Cuisine Features - Multiple Cuisine'>
                Cuisine Features - Multiple Cuisine
              </option>
              <option value='Restaurant Timings - Seating Area'>
                Restaurant Timings - Seating Area
              </option>
              <option value='Subscription Details - Casual Dining Seating Area'>
                Subscription Details - Casual Dining Seating Area
              </option>
              <option value='Subscription Details - FSR Seating Area'>
                Subscription Details - FSR Seating Area
              </option>
            </select>
          </div>
          <TextAreaComponent
            labeltext='Comments'
            inputlabelclass={'input-label-suggest'}
            name={'suggest_comment'}
            type={'text'}
            onChange={this.onChange}
            value={this.state.suggest_comment}
            place={'eg. A bright Atmosphere'}
            error={errors.suggest_comment}
            Textareaclass={classnames('form-control textarea-custom', {
              invalid: errors.suggest_comment
            })}
          />
          {/** */}
          <div className='w-75 ml-auto d-flex justify-content-between'>
            <ButtonComponent
              buttontext='Cancel'
              buttontype='button'
              buttonclass='btn button-main button-white'
              onClick={this.modalToggler}
            />
            <ButtonComponent
              buttontext='Submit'
              buttontype='submit'
              buttonclass='btn button-main button-orange ml-3'
              onClick={this.onSuggestSubmit}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div className='suggest_new_main_div' onClick={this.modalToggler}>
          <div className='circle_icons'>
            <i
              className='fa fa-plus'
              style={{ color: 'red' }}
              aria-hidden='true'
            ></i>
          </div>
          <span className='suggest_new_title'>Suggest New</span>
        </div>
        <Modal
          show={this.state.modalShow}
          size='md'
          onHide={this.modalToggler}
          centered
        >
          <Modal.Body className='p-0'>
            <div className='suggest-new-title'>
              <h3>Suggest New</h3>
            </div>
            <form>{this.rendreSuggestButtonModal()}</form>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { create_new_restraunt_type, create_new_suggest_category }
)(withRouter(SuggestNew));
