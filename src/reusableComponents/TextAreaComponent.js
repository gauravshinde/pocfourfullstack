import React from 'react';

const TextAreaComponent = ({
  name,
  labeltext,
  type,
  onChange,
  Textareaclass,
  place,
  value,
  inputlabelclass,
  disabled
}) => {
  return (
    <React.Fragment>
      <div className='form-group'>
        <label className={inputlabelclass} htmlFor={name}>
          {labeltext}
        </label>

        <textarea
          id={name}
          name={name}
          type={type}
          value={value}
          className={Textareaclass}
          placeholder={place}
          // cols='40'
          rows='5'
          onChange={onChange}
          disabled={disabled === true ? { disabled } : false}
        />
      </div>
    </React.Fragment>
  );
};

export default TextAreaComponent;
